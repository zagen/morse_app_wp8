﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MorseApp.General;
using Microsoft.Phone.BackgroundAudio;
using System.Threading;
using System.Windows.Threading;
using System.Diagnostics;

namespace MorseApp
{
    public partial class BonusPage : PhoneApplicationPage
    {
        private Timer mTimer = null;
        private TimeSpan mTrackDuration = TimeSpan.FromSeconds(338);
        public BonusPage()
        {
            InitializeComponent();
            this.AddAds(LayoutRoot, 2);
            BackgroundAudioPlayer.Instance.PlayStateChanged += new EventHandler(audio_StateChanged);
 

            StartTimer();
          }

        private void audio_StateChanged(object sender, EventArgs e)
        {

           if (BackgroundAudioPlayer.Instance.PlayerState == PlayState.Playing)
           {
              RadioAnimationStoryBoard.Begin();
              ProgressSlider.Maximum = BackgroundAudioPlayer.Instance.Track.Duration.TotalSeconds;
              try
              {
                 mTrackDuration = BackgroundAudioPlayer.Instance.Track.Duration;
              }
              finally
              {
                  TotalTimeText.Text = String.Format(@"{0:00}:{1:00}",
                                mTrackDuration.Minutes, mTrackDuration.Seconds);
              }

           }
           else if (BackgroundAudioPlayer.Instance.PlayerState == PlayState.Stopped || BackgroundAudioPlayer.Instance.PlayerState == PlayState.Paused)
           {
               RadioAnimationStoryBoard.Stop();
           }
        }

        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            if (PlayState.Playing == BackgroundAudioPlayer.Instance.PlayerState)
            {
                BackgroundAudioPlayer.Instance.Pause();
            }
            else
            {
                
                BackgroundAudioPlayer.Instance.Play();
            }
        }


        private void ProgressSlider_ManipulationStarted(object sender, System.Windows.Input.ManipulationStartedEventArgs e)
        {
            this.mTimer.Dispose();
            this.mTimer = null;
        }

        private void ProgressSlider_ManipulationCompleted(object sender, System.Windows.Input.ManipulationCompletedEventArgs e)
        {

            if (BackgroundAudioPlayer.Instance.CanSeek)
            {
                BackgroundAudioPlayer.Instance.Position = TimeSpan.FromSeconds(ProgressSlider.Value);
            }
            StartTimer();
            
        }

        private void StartTimer()
        {
            mTimer =  new Timer(delegate
            {
                if (BackgroundAudioPlayer.Instance.Track != null)
                {
                    Dispatcher.BeginInvoke(delegate
                    {
                        TimeSpan cp = BackgroundAudioPlayer.Instance.Position;
                        this.CurrentTimeText.Text = string.Format("{0:00}:{1:00}", cp.Minutes, cp.Seconds);
                        ProgressSlider.Value = cp.TotalSeconds;
                    });
                }

            }, null, 500, 500);
        }

        private void Back()
        {
            BackgroundAudioPlayer.Instance.Stop();
            if (mTimer != null)
            {
                mTimer.Dispose();
            }
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Back();
        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            this.Back();
        }

        private void ReloadButton_Click(object sender, RoutedEventArgs e)
        {
            BackgroundAudioPlayer.Instance.Stop();
            BackgroundAudioPlayer.Instance.Position = TimeSpan.FromSeconds(0);
            BackgroundAudioPlayer.Instance.Play();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Back();
        }
    }
    
}