﻿using System.Windows;
using Microsoft.Phone.Controls;
using MorseApp.General;
using MorseApp.Resources;
using System.Threading;
using System;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using MorseApp.Achievements;
using System.Collections.Generic;

namespace MorseApp
{
    public partial class TransmitTimeTestPage : PhoneApplicationPage
    {
        private Timer timer;
        private ObservableCollection<ResultRecord> resultsData = new ObservableCollection<ResultRecord>();
        private TextCodec codec = new TextCodec();
        private Generator generator;
        private int CorrectCount = 0;
        private int TotalCount = 0;
        public TransmitTimeTestPage()
        {
            InitializeComponent();
            this.AddAds(LayoutRoot, 2);
            this.touchPad.SetEnabled(false);
            this.touchPad.MorseCodeInputed = this.CharInputed;
            this.generator = new Generator();

            if (Settings.Instance.Locale == TextCodec.LOCALE.RUS)
            {
                generator.SetAbc(Generator.abcRus);
            }
            else
            {
                generator.SetAbc(Generator.abcInt);
            }
            this.generator.Size = 1;

        }

        private int TotalTime = 60;
        private bool IsTestInProgress = false;
        private void GeneralViewButton_Click(object sender, RoutedEventArgs e)
        {
            if (!IsTestInProgress)
            {
                if (MessageBox.Show(AppResources.TimeTestDialogDesc, AppResources.AreYouReady, MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                {
                    //reset timer
                    TotalTime = 60;
                    //reset collection with resuts 
                    resultsData.Clear();
                    //enable touchpad and generate first simbol
                    touchPad.SetEnabled(true);
                    IsTestInProgress = true;
                    GeneralViewText.Text = generator.ToString();
                    CorrectCount = 0;
                    TotalCount = 0;
                    StatisticText.Opacity = 1;
                    timer = new Timer(delegate
                    {
                        if (TotalTime <= 0)
                        {
                            timer.Dispose();
                            timer = null;
                            IsTestInProgress = false;
                        }
                        else
                        {
                            Dispatcher.BeginInvoke(delegate
                            {
                                this.TimerText.Text = string.Format("{0:00}:{1:00}", TotalTime / 60, TotalTime % 60);
                            });
                        }
                        TotalTime--;

                    }, null, 1000, 1000);
                }
            }
        }

        private void CharInputed(char code)
        {
            if (code == ' ')
            {
                TotalCount++;
                codec.SetMorse(morseView.MorseCode);
                string inputed = codec.GetDecodedText().ToUpper();
                string correct = GeneralViewText.Text;
                morseView.Clear();

                resultsData.Add(new ResultRecord
                {
                    Correct = correct,
                    Inputed = inputed
                });

                if (correct.Equals(inputed))
                {
                    CorrectCount++;
                }
                StatisticText.Text = string.Format("{0}/{1}", CorrectCount, TotalCount);

                //check if time is up
                if( IsTestInProgress == false )
                {
                    TimerText.Text = AppResources.ShowResult;
                    GeneralViewText.Text = AppResources.Restart;
                    touchPad.SetEnabled(false);
                    //check if new achievements earned
                    List<Achievement> newAchievements = new AchievementsManager().New(CorrectCount, TotalCount);
                    if (newAchievements.Count > 0)
                    {
                        this.NavigationService.Navigate(new Uri("/AchievementsPage.xaml?NewAchievements=" + JsonConvert.SerializeObject(newAchievements), UriKind.Relative));
                    }
                }
                else
                {
                    GeneralViewText.Text = generator.ToString();
                }
            }
            else if (code == TouchPad.LONG_SPACE)
            {
                return;
            }
            morseView.Push(code);
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }

        private void TimerText_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!IsTestInProgress && resultsData.Count > 0)
            {
                this.NavigationService.Navigate(new Uri("/ResultsPage.xaml?data=" + JsonConvert.SerializeObject(this.resultsData) + "&ReturnToMainMenu=False", UriKind.Relative));
            }
        }
    }
}