﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MorseApp.General;
using System.Collections.ObjectModel;
using System.Text;
using MorseApp.Resources;

namespace MorseApp
{
    public partial class SelectSymbolsPage : PhoneApplicationPage
    {

        private Settings _Settings;
        public SelectSymbolsPage()
        {
            InitializeComponent();

            _Settings = Settings.Instance;
            var collection = new ObservableCollection<Character>();

            if (_Settings.Locale == TextCodec.LOCALE.RUS)
            {
                foreach (char symbol in Generator.abcRus)
                {
                    Character character = new Character()
                    {
                        Symbol = symbol.ToString()
                    };

                    collection.Add(character);
                }
                SymbolsList.ItemsSource = collection;
                foreach (Character character in collection)
                {
                    if (_Settings.RusAbc.Contains(character.Symbol))
                    {
                        SymbolsList.SelectedItems.Add(character);
                    }
                }
            }
            else
            {
                foreach (char symbol in Generator.abcInt)
                {
                    Character character = new Character()
                    {
                        Symbol = symbol.ToString()
                    };

                    collection.Add(character);
                }
                SymbolsList.ItemsSource = collection;
                foreach (Character character in collection)
                {
                    if (_Settings.IntAbc.Contains(character.Symbol))
                    {
                        SymbolsList.SelectedItems.Add(character);
                    }
                }

            }

            SymbolsTypeListPicker.ItemsSource = new List<string>()
            {
                AppResources.Symbols,
                AppResources.Words
            };
            if (_Settings.SequenceType == Generator.SequenceType.CHARS)
            {
                SymbolsTypeListPicker.SelectedIndex = 0;
            }
            else
            {
                SymbolsTypeListPicker.SelectedIndex = 1;
            }
            UpdateAbcList();
        }

        private void SymbolsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SymbolsList.SelectedItems.Count <= 0)
            {
                foreach(var item in e.RemovedItems)
                    SymbolsList.SelectedItems.Add(item);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Back();
        }

        private void Back()
        {
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Back();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Character character in SymbolsList.SelectedItems)
            {
                sb.Append(character.Symbol);
            }
            if (_Settings.Locale == TextCodec.LOCALE.RUS)
            {
                _Settings.RusAbc = sb.ToString();
            }
            else
            {
                _Settings.IntAbc = sb.ToString();
            }
            if (SymbolsTypeListPicker.SelectedIndex == 0)
            {
                _Settings.SequenceType = Generator.SequenceType.CHARS;
            }
            else
            {
                _Settings.SequenceType = Generator.SequenceType.WORDS;
            }
            _Settings.Sync();
            Back();
        }

        private void SymbolsTypeListPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateAbcList();
        }

        private void UpdateAbcList()
        {
            if (SymbolsTypeListPicker.SelectedIndex == 0)
            {
                SymbolsList.IsEnabled = true;
            }
            else
            {
                SymbolsList.IsEnabled = false;
            }
        }
    }
    public class Character
    {
        public string Symbol { get; set; }
    }
}