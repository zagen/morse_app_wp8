﻿using System.Windows;
using Microsoft.Phone.Controls;
using MorseApp.General;
using System.Collections.ObjectModel;
using System;
using Newtonsoft.Json;
using MorseApp.Resources;
using System.Diagnostics;

namespace MorseApp
{
    public partial class TransmitTouchpadPage : PhoneApplicationPage
    {
        private ObservableCollection<ResultRecord> resultsData = new ObservableCollection<ResultRecord>();
        private Generator generator;
        private TextCodec codec = new TextCodec();
        private Settings _Settings;
        public TransmitTouchpadPage()
        {
            InitializeComponent();
            this.AddAds(LayoutRoot, 2);

            generator = new Generator();
            touchPad.MorseCodeInputed = InputedChar;
            _Settings = Settings.Instance;
            _Settings.PropertyChanged += _Settings_PropertyChanged;
            Reload();

        }

        void _Settings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("SequenceType"))
            {
                generator.Type = _Settings.SequenceType;
                Reload();
            }
            else if (e.PropertyName.Equals("IntAbc"))
            {
                generator.SetAbc(_Settings.IntAbc);
                Reload();
            }
            else if (e.PropertyName.Equals("RusAbc"))
            {
                generator.SetAbc(_Settings.RusAbc);
                Reload();
            }
        }

        private int SpacesInputed = 0;
        private void InputedChar(char code)
        {
            if (code == TouchPad.LONG_SPACE)
                return;
            morseView.Push(code);
            if (code == ' ')
            {
                SpacesInputed++;
                if (SpacesInputed == GeneralViewText.Text.Length)
                {
                    touchPad.SetEnabled(false);
                    SpacesInputed = 0;
                    codec.SetMorse(morseView.MorseCode.Trim());
                    string correct = GeneralViewText.Text;
                    string inputed = codec.GetDecodedText().ToUpper();
                    
                    if (correct.Equals(inputed))
                    {
                        Reload(AppResources.AnswerIsCorrect);
                    } 
                    else
                    {
                        StatusText.ShowPopup(AppResources.WrongAnswer);
                        CorrectAnswerTitleText.Opacity = 1;
                        codec.SetText(correct);
                        morseView.Clear();
                        morseView.Push(codec.GetEncodedText());
                        GeneralViewText.CompareTextBlockWithStringClearInlines(inputed);
                    }
                    this.resultsData.Add(new ResultRecord
                    {
                        Correct = correct,
                        Inputed = inputed
                    });
                }
            }

            
        }

        private void Back()
        {
            this.touchPad.Destroy();
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Back();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.resultsData.Count >0)
            {
                //here segue to results page

                this.NavigationService.Navigate(new Uri("/ResultsPage.xaml?data=" + JsonConvert.SerializeObject(this.resultsData), UriKind.Relative));
            }
            else
            {
                this.Back();
            }
        }
        public void Reload(string message = "")
        {
            SpacesInputed = 0;
            GeneralViewText.Text = generator.ToString();
            morseView.Clear();
            if (message.Length > 0)
            {
                message = string.Format("{0}\n{1}", message, AppResources.NewSequenceGenerated);
            }
            else
            {
                message = AppResources.NewSequenceGenerated;
            }
            StatusText.ShowPopup(message);
            touchPad.SetEnabled(true);
        }


        private void ReloadButton_Click(object sender, RoutedEventArgs e)
        {
            Reload();
        }

        private void GeneralViewButton_Click(object sender, RoutedEventArgs e)
        {

            SpacesInputed = 0;
            CorrectAnswerTitleText.Opacity = 0;
            morseView.Clear();
            string generated = this.GeneralViewText.Text;
            this.GeneralViewText.Inlines.Clear();
            this.GeneralViewText.Text = generated;
            touchPad.SetEnabled(true);
            StatusText.ShowPopup(AppResources.TryAgain);
        }

    }
}