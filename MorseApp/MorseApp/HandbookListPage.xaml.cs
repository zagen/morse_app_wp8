﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using MorseApp.General;

namespace MorseApp
{
    public partial class HandbookListPage : PhoneApplicationPage
    {
        ObservableCollection<HandbookElement> _HandbookData = new ObservableCollection<HandbookElement>();
        public HandbookListPage()
        {
            InitializeComponent();
            _HandbookData = new Handbook().Elements;
            list.DataContext = _HandbookData;
            this.AddAds(LayoutRoot, 2);

        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }

        private void list_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (list.SelectedIndex == -1)
                return;
            //Goto detailed page
            NavigationService.Navigate(new Uri("/HandbookDetailsPage.xaml?Item=" + list.SelectedIndex, UriKind.Relative));
            //reset selected list item
            list.SelectedIndex = -1;
        }
    }

}