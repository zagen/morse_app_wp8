﻿using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using MorseApp.General;
using GoogleAds;

namespace MorseApp
{
    public partial class ReceiveExersPage : PhoneApplicationPage
    {
        public ReceiveExersPage()
        {
            InitializeComponent();
            this.AddAds(LayoutRoot, 2);
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }
    }
}