﻿using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using MorseApp.General;
using GoogleAds;
using System.Windows.Navigation;

namespace MorseApp
{
    public partial class MainPage : PhoneApplicationPage
    {
        private InterstitialAd interstitialAd;
  

        private void OnAdReceived(object sender, AdEventArgs e)
        {
            interstitialAd.ShowAd();
        }

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            this.AddAds(LayoutRoot, 2);

            if (!Settings.Instance.AdsRemoved)
            {
                interstitialAd = new InterstitialAd("ca-app-pub-4530434209938102/2526714874");

                ShowInterstitialAds();
            }
            
            WordAccessor.Instance.GetRandomWord(TextCodec.LOCALE.RUS);
           
        }

        private void ShowInterstitialAds()
        {
            AdRequest adRequest = new AdRequest();
            interstitialAd.ReceivedAd += OnAdReceived;
            interstitialAd.LoadAd(adRequest);
        }

    }
}