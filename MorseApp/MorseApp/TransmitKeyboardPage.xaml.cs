﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using GoogleAds;
using MorseApp.General;
using MorseApp.Resources;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Collections.ObjectModel;

namespace MorseApp
{
    public partial class TransmitKeyboardPage : PhoneApplicationPage
    {
        private Settings _Settings;
        private General.Generator generator = new General.Generator();
        private TextCodec codec = new TextCodec();
        private ObservableCollection<ResultRecord> resultsData = new ObservableCollection<ResultRecord>();
        public TransmitKeyboardPage()
        {
            InitializeComponent();

            this.AddAds(LayoutRoot, 2);
            this.Reload();
            _Settings = Settings.Instance;
            _Settings.PropertyChanged += _Settings_PropertyChanged;

        }

        void _Settings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("SequenceType"))
            {
                generator.Type = _Settings.SequenceType;
                Reload();
            }
            else if (e.PropertyName.Equals("IntAbc"))
            {
                generator.SetAbc(_Settings.IntAbc);
                Reload();
            }
            else if (e.PropertyName.Equals("RusAbc"))
            {
                generator.SetAbc(_Settings.RusAbc);
                Reload();
            }
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private void DotButton_Click(object sender, RoutedEventArgs e)
        {
            this.morseView.Push('.');
        }

        private void DashButton_Click(object sender, RoutedEventArgs e)
        {
            this.morseView.Push('-');
        }

        private void SpaceButton_Click(object sender, RoutedEventArgs e)
        {
            this.morseView.Push(' ');
        }

        private void ReloadButton_Click(object sender, RoutedEventArgs e)
        {
            this.StatusText.ShowPopup(AppResources.NewSequenceGenerated);
            this.Reload();
        }
        private void Reload()
        {
            this.GeneralViewText.Text = this.generator.ToString().ToUpper();
            this.CorrectAnswerTitleText.Opacity = 0;
            this.morseView.Clear();
            EnableKeyboard(true);

        }
        private void EnableKeyboard(bool isEnabled)
        {
            this.DotButton.IsEnabled = isEnabled;
            this.DashButton.IsEnabled = isEnabled;
            this.SpaceButton.IsEnabled = isEnabled;
            this.EnterButton.IsEnabled = isEnabled;
        }
        private void EnterButton_Click(object sender, RoutedEventArgs e)
        {
            EnableKeyboard(false);

            this.codec.SetMorse(this.morseView.MorseCode.Trim());
            string generated = this.GeneralViewText.Text;
            string inputed = this.codec.GetDecodedText().ToUpper();
            //put data in result data container for further showing it on Results page
            this.resultsData.Add(new ResultRecord
            {
                Correct = generated,
                Inputed = inputed
            });

            int comparationResult = this.GeneralViewText.CompareTextBlockWithStringClearInlines(this.codec.GetDecodedText());

            if (comparationResult == 0)
            {
                this.StatusText.ShowPopup(AppResources.AnswerIsCorrect + '\n' + AppResources.NewSequenceGenerated);
                this.Reload();
            }else
            {
                string information = "";
                if(comparationResult == 1)
                {
                    information += AppResources.InputLessRequired;
                }else if(comparationResult == -1)
                {
                    information += AppResources.InputMoreRequired;
                }
                information += "\n" + AppResources.WrongAnswer;
                this.StatusText.ShowPopup(information);
                this.CorrectAnswerTitleText.Opacity = 1.0;
                this.codec.SetText(this.GeneralViewText.Text);
                this.morseView.Clear();
                this.morseView.Push(this.codec.GetEncodedText());
            }
        }

        private void GeneralViewButton_Click(object sender, RoutedEventArgs e)
        {
            this.morseView.Clear();
            this.CorrectAnswerTitleText.Opacity = 0;
            this.StatusText.ShowPopup(AppResources.TryAgain);
            string generated = this.GeneralViewText.Text;
            this.GeneralViewText.Inlines.Clear();
            this.GeneralViewText.Text = generated;
            EnableKeyboard(true);
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if(this.resultsData.Count > 0)
            {
                //here segue to results page

                this.NavigationService.Navigate(new Uri("/ResultsPage.xaml?data=" + JsonConvert.SerializeObject(this.resultsData), UriKind.Relative));
            }else
            {
                this.NavigationService.GoBack();
            }
        }
    }
}