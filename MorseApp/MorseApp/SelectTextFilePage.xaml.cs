﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.ComponentModel;
using System.Diagnostics;
using MorseApp.General;

namespace MorseApp
{
    public partial class SelectTextFilePage : PhoneApplicationPage
    {
        public SelectTextFilePage()
        {
            try
            {
                InitializeComponent();
                FilesControl.PropertyChanged += FileControl_PropertyChanged;
            }
            catch { }
            
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Back();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if (FilesControl.SelectedFile.Length > 0)
            {
                Settings.Instance.Filename = FilesControl.SelectedFile;
                Settings.Instance.Sync();
                Back();
            }
        }
        private void FileControl_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedFile")
            {
                if (FilesControl.SelectedFile.Length > 0)
                {
                    OkButton.IsEnabled = true;
                }
                else
                {
                    OkButton.IsEnabled = false;
                }
            }
        }

        private void Back()
        {
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Back();
        }


    }
}