﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MorseApp.General;
using MorseApp.Transmitters;
using MorseApp.Resources;
using System.Threading;

namespace MorseApp
{
    public partial class HandbookDetailsPage : PhoneApplicationPage
    {
        private Handbook _Handbook;
        private Transmitter _Transmitter;

        public HandbookDetailsPage()
        {
            InitializeComponent();
            this.AddAds(LayoutRoot, 2);
            _Handbook = new Handbook();
            _Transmitter = new ToneTransmitter();

            if (Settings.Instance.IsMnemonicsShowed)
            {
                MnemonicTextTitle.Opacity = 1;
                MnemonicView.Opacity = 1;
            }

            Loaded += delegate
            {
                StatusText.ShowPopup(AppResources.HandbookGreetings);
            };
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string itemString;

            if (NavigationContext.QueryString.TryGetValue("Item", out itemString))
            {
                int item;

                if (int.TryParse(itemString, out item))
                {
                    _Handbook.CurrentElementIndex = item;
                }
            }

            UpdateUI();
        }

        private void OnFlick(object sender, FlickGestureEventArgs e)
        {
            if (e.HorizontalVelocity < 0)
            {
                Next();
            }

            // User flicked towards right
            if (e.HorizontalVelocity > 0)
            {
                // Load the previous symbol data
                Previous();
            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }

        private void PreviousButton_Click(object sender, RoutedEventArgs e)
        {
            Previous();
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            Next();
        }
        private void Next()
        {
            AchievementSlideIn.Begin();
            _Handbook.NextElement();
            UpdateUI();
        }
        private void Previous()
        {
            AchievementSlideOut.Begin();
            _Handbook.PreviousElement();
            UpdateUI();
        }

        private void UpdateUI()
        {
            HandbookElement element = _Handbook.CurrentElement();
            HandbookSymbolName.Text = element.SymbolName;
            SymbolText.Text = element.Symbol;
            MorseCodeView.MorseCode = element.Code;
            MnemonicTextValue.Text = element.Mnemonic;
            
        }

        private void SymbolTextView_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Play();
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            Play();
        }
        private void Play()
        {
            if (_Transmitter.IsPlaying)
            {
                _Transmitter.Stop();
            }
            string symbol = _Handbook.CurrentElement().Symbol;
            _Transmitter.Transmit(symbol);
            StatusText.ShowPopup(AppResources.PlayingCode + " " + symbol);
        }
    }
}