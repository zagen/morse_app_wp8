﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MorseApp.Resources;
using MorseApp.General;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Media;
using System.Globalization;
using MorseApp.Transmitters;
using System.Windows.Media.Imaging;

namespace MorseApp
{
    public partial class ListeningTextPage : PhoneApplicationPage
    {
        private Settings _Settings;
        private string _CurrentWord;
        
        private TextFileWordReader _WordReader;
        private Transmitter _Transmitter;
        private bool _IsPlayNext = false;
        private bool _IsPlay = false;
        private BitmapImage playIcon = new BitmapImage(new Uri(@"/Assets/Icons/text_play.png", UriKind.Relative));
        private BitmapImage pauseIcon = new BitmapImage(new Uri(@"/Assets/Icons/text_pause.png", UriKind.Relative));
        
        public ListeningTextPage()
        {
            InitializeComponent();
            _CurrentWord = "";
            this.AddAds(LayoutRoot, 2);
            _Settings = Settings.Instance;

            _WordReader = new TextFileWordReader();
            _WordReader.FileLoaded = FileLoaded;
            _Transmitter = new ToneTransmitter();

            _Transmitter.TransmissionFinished = delegate
            {
                if (_IsPlayNext)
                {
                    string word = "";
                    try
                    {
                        word = _WordReader.NextWord();
                    }
                    catch { }
                    if (word.Length > 0 && !word.Equals(_CurrentWord))
                    {
                        _CurrentWord = word;
                        Play();
                    }
                    else
                    {
                        _IsPlayNext = false;
                        _IsPlay = false;
                        Stop();
                    }
                }
                else
                {
                    RadioAnimationStoryBoard.Stop();
                    PlayButtonImage.Source = playIcon;
                }
            }; 

            Loaded += async delegate
            {
                if (_Settings.Filename.Length > 0)
                {
                    await _WordReader.LoadFile(_Settings.Filename);
                }
               
            };
            
            _Settings.PropertyChanged += Settings_PropertyChanged;
        }

        private void FileLoaded(bool IsLoaded)
        {
            if(IsLoaded)
            {
                _CurrentWord = _WordReader.CurrentWord().Trim() + " ";
                StatusText.ShowPopup(AppResources.FileOpened);
                UpdateUi();
            }
            else
            {
                StatusText.ShowPopup(AppResources.CantOpenSelectedFile);
            }
        }

        private async void Settings_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Filename" && _Settings.Filename.Length > 0)
            {
                Settings settings = ((Settings)sender);
                settings.TextFilePosition = 0;
                settings.Sync();
                await _WordReader.LoadFile(_Settings.Filename);
            }else if(e.PropertyName.Equals("TextFilePosition"))
            {
                UpdateUi();
            }
        }
        private void Back()
        {
            //put here stoping transmitter
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
            Stop();
        }

        private void Forward()
        {
            _IsPlayNext = false;
            _Transmitter.Stop();
            try
            {
                string next = _WordReader.NextWord();
                if (next.Length > 0 && !next.Equals(_CurrentWord))
                {
                    _CurrentWord = next;
                    CurrentWordText.Text = _CurrentWord;
                    UpdateUi();

                    if(_IsPlay)
                        Play();
                    
                }
                
            }
            catch
            {
                MessageBox.Show(AppResources.FileNotSelected);
            }
        }
        private void Backward()
        {
            try
            {
                _IsPlayNext = false;
                _Transmitter.Stop();

                string previous = _WordReader.PreviousWord();
                if (previous.Length > 0 && !previous.Equals(_CurrentWord))
                {
                    _CurrentWord = previous;
                    CurrentWordText.Text = _CurrentWord;
                    UpdateUi();
                    if(_IsPlay)
                        Play();
                }
            }
            catch
            {
                MessageBox.Show(AppResources.FileNotSelected);
            }
        }
        private void Play()
        {
            if (_CurrentWord.Length > 0)
            {
                RadioAnimationStoryBoard.Begin();
                _Transmitter.Transmit(_CurrentWord);
                _IsPlayNext = true;
                PlayButtonImage.Source = pauseIcon;
                _IsPlay = true;
            }
        }
        private void Stop()
        {
            _IsPlayNext = false;
            _IsPlay = false;
            _Transmitter.Stop();
        }

        private const int FAST_CONSTANT = 10;

        private void FastForward()
        {
            try
            {
                _IsPlayNext = false;
                _Transmitter.Stop();

                for (int i = 0; i < FAST_CONSTANT; i++)
                {
                    this._WordReader.NextWord();
                }
                string fastforwardword = _WordReader.CurrentWord();
                if (!fastforwardword.Equals(_CurrentWord))
                {
                    _CurrentWord = fastforwardword;
                    CurrentWordText.Text = _CurrentWord;
                    UpdateUi();
                    if (_IsPlay)
                        Play();
                }
                
            }
            catch
            {
                MessageBox.Show(AppResources.FileNotSelected);
            }
        }
        private void FastBackward()
        {
            _IsPlayNext = false;
            _Transmitter.Stop();
            try
            {
                for (int i = 0; i < FAST_CONSTANT; i++)
                {
                    this._WordReader.PreviousWord();
                }
                string fastbackwardword = _WordReader.CurrentWord();
                if (!fastbackwardword.Equals(_CurrentWord))
                {
                    _CurrentWord = fastbackwardword;
                    CurrentWordText.Text = _CurrentWord;
                    UpdateUi();
                    if(_IsPlay)
                        Play();
                }

            }
            catch
            {
                MessageBox.Show(AppResources.FileNotSelected);
            }
        }
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Back();
        }

        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            if (_IsPlay)
            {
                Stop();
            }
            else
            {
                Play();
            }
            
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            Back();
        }

        private void TextBlock_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/SelectTextFilePage.xaml", UriKind.Relative));
        }

        private void OpenFileButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/SelectTextFilePage.xaml", UriKind.Relative));
        }


        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            if (_IsPlay)
            {
                Stop();
            }
            else
            {
                Play();
            }
        }

        private void PlayForwardButton_Click(object sender, RoutedEventArgs e)
        {
            Forward();
        }

        private void PlayBackwardButton_Click(object sender, RoutedEventArgs e)
        {
            Backward();
        }
        

        private void PlayForwardFastButton_Click(object sender, RoutedEventArgs e)
        {
            FastForward();
        }

        private void PlayBackwardFastButton_Click(object sender, RoutedEventArgs e)
        {
            FastBackward();
        }

        private void GoToPercentButton_Click(object sender, RoutedEventArgs e)
        {
            if (_WordReader.Filename == null || _WordReader.Filename.Length <= 0)
                return;

            TextBox textBox = new TextBox();

            Utilites util = new Utilites();
            InputScope scope = new InputScope();
            InputScopeName name = new InputScopeName();
            SolidColorBrush orange = new SolidColorBrush(Colors.Orange);
            SolidColorBrush white = new SolidColorBrush(Colors.White);
            name.NameValue = InputScopeNameValue.Number;
            scope.Names.Add(name);
            textBox.InputScope = scope;
            textBox.TextChanged += delegate(object sen, TextChangedEventArgs ev)
            {
                
                double result = 0;
                if (util.IsDouble(textBox.Text,  out result))
                {
                    if(result >=0 && result < 100)
                        textBox.Background = white;
                    else
                        textBox.Background = orange;
                }
                else
                {
                    textBox.Background = orange;
                }
            };
            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Message = AppResources.GotoPercent,
                Content = textBox,
                LeftButtonContent = AppResources.Cancel,
                RightButtonContent = "Ok",
            };

            messageBox.Dismissing += (s1, e1) =>
            {
                if (e1.Result == CustomMessageBoxResult.RightButton)
                {
                    double result = 0;
                    if (util.IsDouble(textBox.Text, out result))
                    {
                        if (result < 0 || result > 100)
                            e1.Cancel = true;
                    }
                    else
                    {
                        e1.Cancel = true;
                    }
                }
                
            };

            messageBox.Dismissed += (s2, e2) =>
            {
                switch (e2.Result)
                {

                    case CustomMessageBoxResult.RightButton:
                    double result = 0;
                    if (util.IsDouble(textBox.Text, out result))
                            {
                                if (result >= 0 && result < 100)
                                {
                                    _IsPlayNext = false;
                                    _Transmitter.Stop();
                                    _WordReader.GoToPercent(result);
                                    _CurrentWord = _WordReader.CurrentWord();
                                    UpdateUi();

                                    if (_IsPlay)
                                        Play();
                                }
                                    
                            }
                        break;
                    default:
                        break;
                }
            };
            messageBox.Show();
        }

        private void UpdateUi()
        {
            try
            {
                FilenameTextBlock.Text = _WordReader.Filename;
                CurrentWordText.Text = _WordReader.CurrentWord();
                CurrentPercentText.Text = String.Format(CultureInfo.InvariantCulture,
                                 "{0:00.00 %}", _WordReader.CurrentPercent);
            }
            catch
            {
                FilenameTextBlock.Text = AppResources.SelectTextFile;
                CurrentWordText.Text = "";
                CurrentPercentText.Text = "00.00 %";
            }
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Stop();
            this.NavigationService.Navigate(new Uri("/HandbookListPage.xaml", UriKind.Relative));
        }
    }

}