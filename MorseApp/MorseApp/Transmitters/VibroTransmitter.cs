﻿using Microsoft.Devices;
using MorseApp.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MorseApp.Transmitters
{
    class VibroTransmitter:Transmitter
    {
        private string Message = "";
        private int Index = 0;
        private int SignalDuration = 1;
        private Thread _Thread;
        private VibrateController _VibrateController;
        
        public VibroTransmitter()
        {
            SignalDuration = (int)Settings.Instance.SignalDurationMS;
            _VibrateController = VibrateController.Default;
        }
        public override void Transmit(string message)
        {
            IsPlaying = true;
            Index = 0;
            Codec.SetText(message);
            Message = Codec.GetEncodedText();
            _Thread = new System.Threading.Thread(Play);
            _Thread.Start();
        }

        public override void Stop()
        {
            IsPlaying = false;
        }

        private void Play()
        {

            while (Index < Message.Length && IsPlaying)
            {
                if (Message[Index] == '-')
                {
                    _VibrateController.Start(TimeSpan.FromMilliseconds(SignalDuration * 3));
                    Thread.Sleep(SignalDuration * 4);
                }
                else if (Message[Index] == '.')
                {
                    _VibrateController.Start(TimeSpan.FromMilliseconds(SignalDuration));
                    Thread.Sleep(SignalDuration * 2);
                }
                else if (Message[Index] == ' ')
                {

                    Thread.Sleep(SignalDuration * 3);
                }
                else
                {
                    Thread.Sleep(SignalDuration * 5);
                }
                Index++;
            }
            
            if (TransmissionFinished != null && ParentPage != null)
            {
                ParentPage.Dispatcher.BeginInvoke(delegate
                {
                    TransmissionFinished();
                });
            }
            IsPlaying = false;    
        }
    }
}
