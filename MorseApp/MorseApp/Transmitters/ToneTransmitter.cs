﻿using Microsoft.Xna.Framework.Audio;
using MorseApp.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorseApp.Transmitters
{
    public class ToneTransmitter:Transmitter
    {
        public override void Transmit(string message)
        {
            if (IsPlaying)
            {
                this.Stop();
            }
            Index = 0;
            Codec.SetText(message);
            this.Message = Codec.GetEncodedText();
            Play();
        }


        private string Message = "";
        private int Index = 0;

        private int[] AudioBufferSize;

        private int CountSoundTypes = 4;//dash, dot, space, long space;

        private const int BytesPerSample = 2;
        private const double MaxWaveMagnitude = 32000d;
        private const int DefaultSampleRate = 22000;

        object DynamicSoundSync = new object ();


        private byte[][] _audioBufferList;
        private short[][] _renderingBuffer;
        private DynamicSoundEffectInstance _dynamicSound;


        public ToneTransmitter()
        {
            double SignalDuration = Settings.Instance.SignalDurationMS;
            _Frequency = Settings.Instance.Frequence;

            AudioBufferSize = new int[CountSoundTypes];

            AudioBufferSize[0] = (int)Math.Ceiling(SignalDuration * 4 * SampleRate / 1000.0);
            AudioBufferSize[1] = (int)Math.Ceiling(SignalDuration * 2 * SampleRate / 1000.0);
            AudioBufferSize[2] = (int)Math.Ceiling(SignalDuration * 2 * SampleRate / 1000.0);
            AudioBufferSize[3] = (int)Math.Ceiling(SignalDuration * 7 * SampleRate / 1000.0);

            _audioBufferList = new byte[CountSoundTypes][];

            _renderingBuffer = new short[CountSoundTypes][];

            for (int i = 0; i < _audioBufferList.Length; i++)
            {
                _audioBufferList[i] = new byte[BytesPerSample * AudioBufferSize[i]];
                _renderingBuffer[i] = new short[AudioBufferSize[i]];
            }
            
            AudioListener x = new AudioListener();
            _Frequency = Settings.Instance.Frequence;
            
            FillBuffer();
            
        }

        double SoundFunction(double time)
        {
            return Math.Sin(Frequency / (double)SampleRate * time * Math.PI * 2.0d);
        }



        void FillBuffer()
        {

            //заполняем 3/4 буфера тире
            byte[] destinationBuffer = _audioBufferList[0];
            short result;

            int lengthOfSignal = destinationBuffer.Length * 3 / (4 * BytesPerSample);
            int lengthOfRamp = (int)((double)lengthOfSignal * 0.2);

            for (int i = 0; i < lengthOfSignal ; ++i)
            {
                int baseIndex = BytesPerSample * i;
                result = (short)(MaxWaveMagnitude * SoundFunction(i));
                    
                _renderingBuffer[0][i] = result;
                   
                
            }
            //ramp for good sound
            //begin ramp
            for (int i = 0; i < lengthOfRamp; i++)
            {
                _renderingBuffer[0][i] = (short)((double)i / ((double)lengthOfRamp) * _renderingBuffer[0][i]);
            }
            int beginRump = (int)((double)lengthOfSignal * 0.8d);

            for (int i = beginRump; i < lengthOfRamp + beginRump; i++)
            {
                _renderingBuffer[0][i] = (short)(((double)lengthOfRamp - ((double)i - beginRump)) / ((double)lengthOfRamp) * _renderingBuffer[0][i]);
            }

            Buffer.BlockCopy(_renderingBuffer[0], 0, destinationBuffer, 0, _renderingBuffer[0].Length * sizeof(short));

            // заполняем половину буфера для точки

            destinationBuffer = _audioBufferList[1];
            lengthOfSignal = destinationBuffer.Length / (2 * BytesPerSample);
            lengthOfRamp = (int)((double)lengthOfSignal * 0.2);


            for (int i = 0; i < lengthOfSignal; ++i)
            {
                int baseIndex = BytesPerSample * i;
                result = (short)(MaxWaveMagnitude * SoundFunction(i));

                _renderingBuffer[1][i] = result;


            }
            for (int i = 0; i < lengthOfRamp; i++)
            {
                _renderingBuffer[1][i] = (short)((double)i / ((double)lengthOfRamp) * _renderingBuffer[1][i]);
            }
            beginRump = (int)((double)lengthOfSignal * 0.8d);

            for (int i = beginRump; i < beginRump +  lengthOfRamp; i++)
            {
                _renderingBuffer[1][i] = (short)(((double)lengthOfRamp - ((double)i - beginRump)) / (double)lengthOfRamp * _renderingBuffer[1][i]);
            }

            Buffer.BlockCopy(_renderingBuffer[1], 0, destinationBuffer, 0, _renderingBuffer[1].Length * sizeof(short));
        }
        public void Play()
        {
            lock (DynamicSoundSync)
            {
                if (_dynamicSound == null)
                {
                    IsPlaying = true;
                    _dynamicSound = new DynamicSoundEffectInstance(SampleRate, AudioChannels.Mono);
                    _dynamicSound.BufferNeeded += BufferNeeded;

                    SubmitBuffer();
                    _dynamicSound.Play();
                }
            }
        }

        public override void Stop()
        {
            Message = "";
            if (_dynamicSound != null)
            {
                lock (DynamicSoundSync)
                {
                    if (_dynamicSound != null)
                    {
                        IsPlaying = false;
                        _dynamicSound.Stop();
                        _dynamicSound.Dispose();
                        _dynamicSound = null;
                    }
                }
            }
            if (TransmissionFinished != null)
            {
                TransmissionFinished();
            }
        }

        void BufferNeeded(object sender, EventArgs args)
        {
            SubmitBuffer();
        }

        void SubmitBuffer()
        {
            int bufferIndex = 0;

            if (Index > Message.Length)
            {
                Stop();
                return;
            }
            else if (Index == Message.Length && Message.Length != 0)
            {
                bufferIndex = 3;
            }
            else
            {
                switch (Message[Index])
                {
                    case '-':
                        bufferIndex = 0;
                        break;
                    case '.':
                        bufferIndex = 1;
                        break;
                    case ' ':
                        bufferIndex = 2;
                        break;
                    default:
                        bufferIndex = 3;
                        break;
                }
            }
            Index++;
            lock (DynamicSoundSync)
            {
                DynamicSoundEffectInstance currentSound = _dynamicSound;
                if (currentSound != null)
                {
                    currentSound.SubmitBuffer(_audioBufferList[bufferIndex]);
                    
                }
            }
            OnPropertyChanged("PendingBufferCount");
        }




       //-----
        // SampleRate - generated from ObservableField snippet - Joel Ivory Johnson
        private int _sampleRate = DefaultSampleRate;
        public int SampleRate
        {
            get { return _sampleRate; }
            set
            {
                if (_sampleRate != value)
                {
                    _sampleRate = value;
                    OnPropertyChanged("SampleRate");
                }
            }
        }


        // Frequency - generated from ObservableField snippet - Joel Ivory Johnson
        private double _Frequency = 1000;
        public double Frequency
        {
            get { return _Frequency; }
            set
            {
                if (_Frequency != value)
                {
                    _Frequency = value;
                    OnPropertyChanged("Frequency");
                }
            }
        }
        //-----
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
