﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using MorseApp.General;
using System.Threading;

namespace MorseApp.Transmitters
{
    public partial class ScreenTransmitterBlankPage : PhoneApplicationPage
    {
        private bool IsAlive = true;
        private SolidColorBrush BlackBrush;
        private SolidColorBrush BrightBrush;
        private string Message = "";
        private int Position = 0;
        private int SignalDuration = 1;
        private Thread _Thread;

        public ScreenTransmitterBlankPage()
        {
            InitializeComponent();
            BlackBrush = new SolidColorBrush(Colors.Black);
            int brightness = Settings.Instance.Brightness;
            byte component = (byte)((double)brightness / 100.0 * 255.0);
            BrightBrush = new SolidColorBrush(Color.FromArgb(255, component, component, component));
            SignalDuration = (int)Settings.Instance.SignalDurationMS;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            IsAlive = false;
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string data = "";
            
            if (NavigationContext.QueryString.TryGetValue("data", out data))
            {
                Message = data;
                _Thread = new System.Threading.Thread(Play);
                _Thread.Start();

            }
        }
        private void Play()
        {
            Thread.Sleep(500);
            while(Position < Message.Length && IsAlive)
            {
                if (Message[Position] == '-')
                {
                    Dispatcher.BeginInvoke(delegate
                    {
                        LayoutRoot.Background = BrightBrush;
                    });
                    Thread.Sleep(SignalDuration * 3);
                    Dispatcher.BeginInvoke(delegate
                    {
                        LayoutRoot.Background = BlackBrush;
                    });
                    Thread.Sleep(SignalDuration);
                }
                else if (Message[Position] == '.')
                {
                    Dispatcher.BeginInvoke(delegate
                    {
                        LayoutRoot.Background = BrightBrush;
                    });
                    Thread.Sleep(SignalDuration);
                    Dispatcher.BeginInvoke(delegate
                    {
                        LayoutRoot.Background = BlackBrush;
                    });
                    Thread.Sleep(SignalDuration);
                }
                else if (Message[Position] == ' ')
                {
                    
                    Thread.Sleep(SignalDuration * 3);
                }
                else
                {
                    Thread.Sleep(SignalDuration * 5);
                }
                Position++;
            }
            
            Dispatcher.BeginInvoke(delegate
            {
                Position = 0;
                if (this.NavigationService.CanGoBack)
                {
                    this.NavigationService.GoBack();
                }
            });

        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            IsAlive = false;
        }
    }
}