﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MorseApp.Transmitters
{
    public class ScreenTransmitter:Transmitter
    {
        
        public override void Transmit(string message)
        {
            Codec.SetText(message);
            if(ParentPage != null)
                ParentPage.NavigationService.Navigate(new Uri("/Transmitters/ScreenTransmitterBlankPage.xaml?data=" + Codec.GetEncodedText(), UriKind.Relative));
            if (TransmissionFinished != null)
            {
                TransmissionFinished();
            }
        }

        public override void Stop()
        {
            
        }
    }
}
