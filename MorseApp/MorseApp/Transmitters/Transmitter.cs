﻿using MorseApp.General;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MorseApp.Transmitters
{
    public abstract class Transmitter
    {
        private TextCodec mCodec;
        public Page ParentPage { get; set; }
    
        public delegate void TransmissionFinishedDelegate();

        public TransmissionFinishedDelegate TransmissionFinished { get; set; }
        public TextCodec Codec
        {
            get
            {
                if(mCodec == null) mCodec = new TextCodec();

                mCodec.Locale = TextCodec.LOCALE.ALL;
                return mCodec;
            }
            set
            {
                this.mCodec = value;
            }
        }

        private bool _isPlaying = false;
        public bool IsPlaying
        {
            get { return _isPlaying; }
            protected set
            {
                if (_isPlaying != value)
                {
                    _isPlaying = value;
                }
            }
        }

        private static List<Type> Transmitters = new List<Type>()
        {
            typeof (ToneTransmitter),
            typeof (ScreenTransmitter),
            typeof (VibroTransmitter)
        };

        public static Transmitter CreateCurrentTransmitter()
        {
            Settings settings = Settings.Instance;
            Transmitter currentTransmitter = null;

            try
            {
                currentTransmitter = (Transmitter)Activator.CreateInstance(Transmitters[settings.Transmitter]);
            }
            catch
            {
                currentTransmitter = new ToneTransmitter();
            }
            return currentTransmitter;
        }
        public abstract void Transmit(string message);
        public abstract void Stop();
    }
    
}
