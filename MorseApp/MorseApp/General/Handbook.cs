﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorseApp.General
{
    public class HandbookElement
    {
        public string Symbol { get; set; }
        public string Code { get; set; }
        public string SymbolName { get; set; }
        public string Mnemonic { get; set; }
    }

    public class Handbook
    {
        private ObservableCollection<HandbookElement> _Elements ;
        private Settings _Settings;
        private TextCodec _Codec;
        public ObservableCollection<HandbookElement> Elements { get { return _Elements; } }
        private int _CurrentElementIndex = 0;
        public int CurrentElementIndex 
        {
            get 
            { 
                return _CurrentElementIndex; 
            }
            set
            {
                if(value >=  0 || value < _Elements.Count)
                    _CurrentElementIndex = value;
            }
        }



        public Handbook()
        {
            _Settings = Settings.Instance;
            _Codec = new TextCodec();
            _Codec.Locale = TextCodec.LOCALE.ALL;
            _Elements = new ObservableCollection<HandbookElement>();
            string[] symbols;
            Dictionary<string, string> mnemonics;
            Dictionary<string, string> names;
            if (_Settings.Locale == TextCodec.LOCALE.RUS)
            {
  
                symbols = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ1234567890.,:;()\"'-/?!@".ToCharArray().Select(c => c.ToString()).ToArray();
                mnemonics = new Dictionary<string, string>()
                {
                    {"А", "ай-даа"},
                    {"Б", "баа-ки-те-кут"},
                    {"В", "ви-даа-лаа"},
                    {"Г", "гаа-раа-ж"},
                    {"Д", "доо-ми-ки"},
                    {"Е", "есть"},
                    {"Ё", "есть"},
                    {"Ж", "я-бук-ва-жее"},
                    {"З", "заа-каа-ти-ки"},
                    {"И", "и-ди"},
                    {"Й", "и-краат-коо-ее"},
                    {"К", "каак-де-лаа"},
                    {"Л", "лу-наа-ти-ки"},
                    {"М", "маа-маа"},
                    {"Н", "ноо-мер"},
                    {"О", "оо-коо-лоо"},
                    {"П", "пи-лаа-поо-ёт"},
                    {"Р", "ре-шаа-ет"},
                    {"С", "са-мо-лёт"},
                    {"Т", "таак"},
                    {"У", "у-нес-лоо"},
                    {"Ф", "фи-ли-моон-чик"},
                    {"Х", "хи-ми-чи-те"},
                    {"Ц", "цаа-пли-наа-ши"},
                    {"Ч", "чее-лоо-вее-чек"},
                    {"Ш", "шаа-роо-ваа-рыы"},
                    {"Щ", "щуу-каа-жи-ваа"},
                    {"Ъ", "ээ-тоо-твёр-дыый-знаак"},
                    {"Ы", "ыы-не-наа-доо"},
                    {"Ь", "тоо-мяг-кий-знаак"},
                    {"Э", "э-ле-ктроо-ни-ки"},
                    {"Ю", "ю-ли-аа-наа"},
                    {"Я", "я-маал-я-маал"},
                          
                    {"1", "и-тооль-коо-оо-днаа"},
                    {"2", "две-не-хоо-роо-шоо"},
                    {"3", "три-те-бе-маа-лоо"},
                    {"4", "че-тве-ри-те-каа"},
                    {"5", "пя-ти-ле-ти-е"},
                    {"6", "поо-шес-ти-бе-ри"},
                    {"7", "даай-даай-за-ку-рить"},
                    {"8", "воо-сьмоо-гоо-и-ди"},
                    {"9", "ноо-наа-ноо-наа-ми"},
                    {"0", "нооль-тоо-оо-коо-лоо"},
                    {".", "то-чеч-ка-то-чеч-ка"},
                    {",", "крю-чоок-крю-чоок-крю-чоок"},
                    {":", "двоо-ее-тоо-чи-е-ставь"},
                    {";", "тоо-чка-заа-пя-таа-я"},
                    {"(", "скоо-бку-стаавь-скоо-бку-стаавь"},
                    {")", "скоо-бку-стаавь-скоо-бку-стаавь"},
                    {"'", "крю-чоок-тыы-веерх-ниий-ставь"},
                    {"\"", "ка-выы-чки-ка-выы-чки"},
                    {"-", "чёёр-точ-ку-мне-да-ваай"},
                    {"/", "дрообь-здесь-пред-стаавь-те"},
                    {"?", "вы-ку-даа-смоо-три-те"},
                    {"!", "оо-наа-вос-кли-цаа-лаа"},
                    {"@", "со-баа-каа-ку-саа-ет"}
                };
            }
            else
            {
                symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.,:;()\"'-/?!@".ToCharArray().Select(c => c.ToString()).ToArray();
                mnemonics = new Dictionary<string, string>()
                {
                     {"A", "al-FA"},
                     {"B", "YEAH! *clap* *clap* *clap*"},
                     {"C", "CHAR-lie's AN-gels"},
                     {"D", "NEW or-leans"},
                     {"E", "hey!"},
                     {"F", "*step**step**BRUSH**step*"},
                     {"G", "HOLE IN one!"},
                     {"H", "ho-li-day-inn"},
                     {"I", "bom-bay"},
                     {"J", "where-FORE ART THOU?"},
                     {"K", "POUND for POUND"},
                     {"L", "li-MA pe-ru"},
                     {"M", "LIKE MIKE"},
                     {"N", "AU-tumn"},
                     {"O", "SUN-NY-DAY"},
                     {"P", "of DOC-TOR good"},
                     {"Q", "A-LOU-et-TUH"},
                     {"R", "ro-MER-o"},
                     {"S", "ne-va-da"},
                     {"T", "*DIP*"},
                     {"U", "u-ni-FORM"},
                     {"V", "the first 4 notes of Beethoven's 5th"},
                     {"W", "jack AND COKE"},
                     {"X", "ON-ly the BONES"},
                     {"Y", "ON a PO-NY"},
                     {"Z", "SHA-KA zu-lu"},
                           
                     {"1", "i-WON-WON-WON-WON"},
                     {"2", "cut-it-SAW-SAW-SAW"},
                     {"3", "make-it-a-THREE-SOME"},
                     {"4", "did-he-hol-ler-FOUR?"},
                     {"5", "sit-a-hal-i-but"},
                     {"6", "SIX-hav-ing-a-fit"},
                     {"7", "SEV-EN-bet-on-it"},
                     {"8", "MOM-IN-LAW-ate-it"},
                     {"9", "NINE-NINE-NINE-NINE-zip"},
                     {"0", "NUL-NUL-NUL-NUL-NUL"},
                     {".", "a STOP a STOP a STOP"},
                     {",", "COM-MA, it's a COM-MA"},
                     {":", "HA-WA-II stan-dard time"},
                     {";", "A-list, B-list, C-list"},
                     {"(", "AU-tumn HOLE-IN-one!"},
                     {")", "AU-tumn A-LOU-et-TUH"},
                     {"'", "and THIS STUFF GOES TO me!"},
                     {"\"", "six-TY-six nine-TY-nine"},
                     {"-", "*DIP* ho-li-day-inn *DIP*"},
                     {"/", "SHAVE and a HAIR-cut"},
                     {"?", "it's a QUES-TION, is it?"},
                     {"!", "AU-tumn ON a PO-NY"},
                     {"@", "al-FA CHAR-lie's AN-gels"}
                };

            }
            names = new Dictionary<string, string>()
            {
                   {"A", "Alpha"},
                   {"B", "Bravo"},
                   {"C", "Charlie"},
                   {"D", "Delta"},
                   {"E", "Echo"},
                   {"F", "Foxtrot"},
                   {"G", "Golf"},
                   {"H", "Hotel"},
                   {"I", "India"},
                   {"J", "Juliet"},
                   {"K", "Kilo"},
                   {"L", "Lima"},
                   {"M", "Mike"},
                   {"N", "November"},
                   {"O", "Oscar"},
                   {"P", "Papa"},
                   {"Q", "Quebec"},
                   {"R", "Romeo"},
                   {"S", "Sierra"},
                   {"T", "Tango"},
                   {"U", "Uniform"},
                   {"V", "Victor"},
                   {"W", "Whiskey"},
                   {"X", "X-ray"},
                   {"Y", "Yankee"},
                   {"Z", "Zulu"}
            };

            foreach (string symbol in symbols)
            {
                _Codec.SetText(symbol);
                string code = _Codec.GetEncodedText();
                string mnemonic = "";
                string name = "";

                if (mnemonics.ContainsKey(symbol))
                {
                    mnemonic = mnemonics[symbol];
                }
                if (names.ContainsKey(symbol))
                {
                    name = names[symbol];
                }

                _Elements.Add(new HandbookElement
                {
                    Symbol = symbol,
                    Code = code,
                    Mnemonic = mnemonic,
                    SymbolName = name
                });
            }
        }

        public HandbookElement CurrentElement()
        {
            if (_CurrentElementIndex >= 0 && _CurrentElementIndex <= _Elements.Count)
                return _Elements[_CurrentElementIndex];
            else
                return _Elements[_CurrentElementIndex];
        }
        public HandbookElement NextElement()
        {
            _CurrentElementIndex++;
            if (_CurrentElementIndex >= _Elements.Count)
            {
                _CurrentElementIndex = 0;
            }
            return _Elements[_CurrentElementIndex];
        }
        public HandbookElement PreviousElement()
        {
            --_CurrentElementIndex;
            if (_CurrentElementIndex < 0)
            {
                _CurrentElementIndex = _Elements.Count - 1;
            }
            return _Elements[_CurrentElementIndex];
        }
    }
}
