﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorseApp.General
{
    public class Generator
    {
        public enum SequenceType { WORDS = 1, CHARS = 0 };
        public int Size { get; set; }
        public TextCodec.LOCALE Locale { get; set; }
        public SequenceType Type { get; set; }

        private Random random = new Random();
        private StringBuilder sb = new StringBuilder();
        public const string abcInt = "abcdefghijklmnopqrstuvwxyz1234567890.,:;()\"'-/?!";
        public const string abcRus = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя1234567890.,:;()\"'-/?!";

        private WordAccessor wordAccessor;
        private const int GROUP_SIZE = 5;
        private string[] groups = new string[]{
            "оеаитнсрвлкetaonrish",
            "мдпуяыгзбчйdlfcmugyp",
            "хъжьюшцщэфёwbvkxjqz",
            "1234567890",
            ".,:;()\"'-/?!"
        };
        private string[] currentGroups = new string[GROUP_SIZE];
        private int[] currentGroupsChance = { 1000, 250, 100, 40, 40 };

        public string abc = "";
        public void SetAbc(string abc)
        {
            //set field abc
            this.abc = abc;

            //here current character from abc in string format will be
            string currentCharacterStr;
    
            //same only in Character format;
            char currentChar;

            //clear currentGroups
            for (int i = 0; i < GROUP_SIZE; i++)
            {
                currentGroups[i] = "";
            }

            //go through all symbols of setting alphabet
            for (int i = 0; i < abc.Length; i++)
            {
                //get current char and convert it to string
                currentChar = abc[i];
                currentCharacterStr = Char.ToString(currentChar);

                //check sequentially all group and if it contains current symbol add it fit current group
                for (int j = 0; j < GROUP_SIZE; j++)
                {
                    if (groups[j].Contains(currentCharacterStr))
                    {
                        currentGroups[j] += currentCharacterStr;
                        break;
                    }
                }
            }
        }

        private void Init()
        {
            Settings settings = Settings.Instance;
            wordAccessor = WordAccessor.Instance;
            for (int i = 0; i < Generator.GROUP_SIZE; i++)
            {
                this.currentGroups[i] = "";
            }
            this.Size = settings.GeneratorLength;
            this.Locale = settings.Locale;
            this.Type = settings.SequenceType;
            if (this.Locale == TextCodec.LOCALE.RUS)
            {
                SetAbc(settings.RusAbc);
            }
            else
            {
                SetAbc(settings.IntAbc);
            }
        }

        public Generator()
        {
            Init();
        }
        public Generator(int size, TextCodec.LOCALE locale)
        {
            Init();
            this.Size = size;
            this.Locale = locale;
        }
        override public string ToString() 
        {
            if (sb.Length != 0)
                sb.Clear();
            //if generated type is words read random word from file
            if (this.Type == Generator.SequenceType.WORDS)
            {
                return wordAccessor.GetRandomWord(this.Locale);
            }

            if (this.Type == Generator.SequenceType.CHARS)
            {
                if(this.abc.Length == 0) //if abc is not set yet set it
                {
                    if (this.Locale == TextCodec.LOCALE.RUS)
                    {
                        SetAbc(abcRus);
                    }
                    else
                    {
                        SetAbc(abcInt);
                    }
                }
                for (int i = 0; i < this.Size; i++)
                {
                    //here will be our generated symbol
				    char generatedChar = ' ';

                   
				    //go down for our groups 
				    for(int j = GROUP_SIZE-1; j >= 0; j--){

                        //generate random number from 0 to 999;
                        int randomNumber = random.Next(1000);

					
					    //check if random number fit probability of current group
					    if(randomNumber < currentGroupsChance[j]){
						    if(!IsGroupEmpty(j)){
							    //if so and current group is not empty, then get random character from group and go out from inner loop
							    generatedChar = GetRandomCharFromGroup(j);
							    break;
						    }
					    }
				    }
				    //if we don't get no one symbol, then get random character from all abc was set
				    if(generatedChar == ' '){
					    generatedChar = abc[random.Next(abc.Length)];
				    }
				    //and add it to current string builder
				    sb.Append(generatedChar);

                }
            }

            return this.sb.ToString().ToUpper();
        }

        public bool IsGroupEmpty(int group)
        {
            return currentGroups[group].Length == 0;
        }

        public char GetRandomCharFromGroup(int group)
        {
            return currentGroups[group][random.Next(currentGroups[group].Length)];
        }
    }
}
