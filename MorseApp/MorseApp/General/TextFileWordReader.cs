﻿using Microsoft.Phone.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MorseApp.General
{
    public class TextFileWordReader
    {
        private char[] SPACES = new char[] { '\t',' ', '\n', '\r'};
        private Settings _Settings;
        
        private int _CurrentPosition;

        private SDCard _SDCard;
        private bool _IsFileLoaded = false;
        private string _TextBuffer;

        private string _Filename;
        public string Filename 
        {
            get
            {
                return _Filename;
            }
        }

        public delegate void FileLoadedDelegate(bool IsLoaded);

        public FileLoadedDelegate FileLoaded { get; set; }

        public TextFileWordReader()
        {
            _Settings = Settings.Instance;
            _SDCard = new SDCard();
            _CurrentPosition = 0;
            
        }


        public async Task LoadFile(string filename)
        {
            _IsFileLoaded = false;
            await _SDCard.GetSDCard();

            _CurrentPosition = _Settings.TextFilePosition;
            ExternalStorageFile file = await _SDCard.GetFileByFullPath(filename);
            _TextBuffer = await _SDCard.ReadAllFile(file);

            _IsFileLoaded =  _TextBuffer.Length > 0;
            if (_IsFileLoaded)
            {
                _Filename = _SDCard.GetFilenameFromFullPath(filename);
            }

            if (FileLoaded != null)
            {
                FileLoaded(_IsFileLoaded);
            }
        }



        private bool FindNextWordPosition(int beginIndex, out int beginWordPosition, out int endWordPosition)
        {
            int index = beginIndex;
            beginWordPosition = index;
            endWordPosition = index;

            while (index < _TextBuffer.Length && SPACES.Contains(_TextBuffer[index])) //pass first spaces, find firs non-space character
                index++;
            beginWordPosition = index;
            while (index < _TextBuffer.Length && !SPACES.Contains(_TextBuffer[index])) //pass non-space chars for finding end of word
                index++;
            endWordPosition = index;
            if (index <= _TextBuffer.Length)
                return (endWordPosition > beginWordPosition); // true if word found
            else
                return false;

        }

        private bool FindPreviousWordPosition(int beginIndex, out int beginWordPosition, out int endWordPosition)
        {
            int index = beginIndex;
            beginWordPosition = index;
            endWordPosition = index;

            do
            {
                index--;
            }
            while (index >= 0 && SPACES.Contains(_TextBuffer[index])); //pass first spaces, find firs non-space character
                
            endWordPosition = index + 1;
            do
            {
                index--;
            }
            while (index >= 0 && !SPACES.Contains(_TextBuffer[index])); //pass non-space chars for finding begin of word
                
            beginWordPosition = index + 1;
            index++;
            if (index >= 0)
                return (endWordPosition > beginWordPosition); // true if word found
            else
                return false;

        }
        public string NextWord()
        {
            if (!_IsFileLoaded)
            {
                throw new FileNotLoadedException();
            }
            int index = _CurrentPosition;
            int beginFirstWord, endFirstWord;
            int beginSecondWord, endSecondWord;

            if (FindNextWordPosition(index, out beginFirstWord, out endFirstWord))
            {
                if (FindNextWordPosition(endFirstWord, out beginSecondWord, out endSecondWord))
                {
                    _CurrentPosition = endFirstWord;
                    SaveProgress();
                    return _TextBuffer.Substring(beginSecondWord, endSecondWord - beginSecondWord).Trim() + ' ';
                }
                else
                {
                    return _TextBuffer.Substring(beginFirstWord, endFirstWord - beginFirstWord).Trim() + ' ';
                }
            }
            else
            {
                return "";
            }

            
        }
        public string PreviousWord()
        {
            if (!_IsFileLoaded)
            {
                throw new FileNotLoadedException();
            }
            int index = _CurrentPosition;
            int begin, end;
            if (FindPreviousWordPosition(index, out begin, out end))
            {
                _CurrentPosition = begin;
                SaveProgress();
                return _TextBuffer.Substring(begin, end - begin).Trim() + ' ';
            }
            else
                return CurrentWord();

        }

        public void GoToPercent(double percent)
        {
            int NewPosition = _CurrentPosition;
            try
            {
                _CurrentPosition = (int)(percent / 100.0 * _TextBuffer.Length);
                while (_CurrentPosition >= 0 && !SPACES.Contains(_TextBuffer[_CurrentPosition]))
                {
                    _CurrentPosition--;
                }
                _CurrentPosition++;
                SaveProgress();
            }
            catch
            {
                _CurrentPosition = NewPosition;
            }
        }
  
        public void SaveProgress()
        {
            _Settings.TextFilePosition = _CurrentPosition;
            _Settings.Sync();
        }

        public string CurrentWord()
        {
            if (!_IsFileLoaded)
            {
                throw new FileNotLoadedException();
            }
            int beginWordPosition, endWordPosition;
            if (FindNextWordPosition(_CurrentPosition, out beginWordPosition,out endWordPosition)) //if it is not end of file , maybe put here call delegate
                return _TextBuffer.Substring(beginWordPosition, endWordPosition - beginWordPosition).Trim() + ' ';
            else
                return "";
        }
        public double CurrentPercent
        {
            get
            {
                return (double)_CurrentPosition / ((double)_TextBuffer.Length - 1);
            }
        }
        
    }
    class FileNotLoadedException : Exception
    {

    }
}
