﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Microsoft.Phone.Storage;
using System.ComponentModel;
using MorseApp.General;

namespace MorseApp
{
    public partial class FileOpenUserControl : UserControl
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<FilesListElement> filesAndDirs = new ObservableCollection<FilesListElement>();
        private SDCard _SDCard;
        private string _SelectedFile = "";
        public string SelectedFile
        {
            get
            {
                return _SelectedFile;
            }
            set
            {
                _SelectedFile = value;
                OnPropertyChanged("SelectedFile");
            }
        }

        
        public FileOpenUserControl()
        {
            InitializeComponent();
            _SDCard = new SDCard();
            Loaded +=  async delegate
            {
                await _SDCard.GetSDCard();
                await UpdateUI();
            };
        }
        public class FilesListElement
        {
            public string Element { get; set; }
        }




        private async Task UpdateUI()
        {
            filesAndDirs = new ObservableCollection<FilesListElement>();
            var dirs = await _SDCard.GetFolders();
            var files = await _SDCard.GetFiles();

            if (dirs != null && files != null)
            {
                filesAndDirs.Add(new FilesListElement
                {
                    Element = ".."
                });

                foreach (var dir in dirs)
                {
                    filesAndDirs.Add(new FilesListElement
                    {
                        Element = dir.Name
                    });
                }


                foreach (var file in files)
                {
                    filesAndDirs.Add(new FilesListElement
                    {
                        Element = file.Name
                    });
                }
            }
            
            list.ItemsSource = filesAndDirs;
        }
 

        private async void list_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (list.SelectedIndex != -1)
            {
                string selected = filesAndDirs.ElementAt(list.SelectedIndex).Element;
                
                if (!selected.EndsWith(".text")) 
                {
                    this.SelectedFile = "";
                    await _SDCard.OpenDir(selected);
                    await UpdateUI();
                }
                else
                {
                    string delim = "";
                    if (_SDCard.CurrentPath.Path.Length > 0)
                        delim = "\\";
                    string selectedFullPath = _SDCard.CurrentPath.Path + delim + selected;

                    if (selected.ToLower().EndsWith(".text"))
                    {
                        this.SelectedFile = selectedFullPath;
                    }
                    
                }
                
            }
        }

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

    }
}
