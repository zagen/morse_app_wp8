﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Shapes;
using System.Windows.Media;

namespace MorseApp.General
{
    public partial class PositionUserControl : UserControl
    {
        private double Size = 4;
        private double HeightMargin = 5;
        private double ControlWidth = 20;

        private int _CurrentPosition = 0;
        public int CurrentPosition
        {
            get
            {
                return _CurrentPosition;
            }
            set
            {
                if (value < 0 )
                {
                    _CurrentPosition = _Total - 1;
                }
                else if (value > (_Total - 1))
                {
                    _CurrentPosition = 0;
                }
                else
                {
                    _CurrentPosition = value;
                }
                Redraw();
            }
        }
        private int _Total = 1;
        public int Total
        {
            get
            {
                return _Total;
            }
            set
            {
                if (value >= 1)
                {
                    _Total = value;
                    CalculateControlWidth();
                }
            }
        }

        private void CalculateControlWidth()
        {
            this.ControlWidth = (_Total - 1) * (this.Size * 2.0) + this.Size * 3.0;
            this.Left = this.PositionCanvas.ActualWidth / 2.0 - this.ControlWidth / 2.0;
            Redraw();
        }
        private double Left = 0;

        public PositionUserControl()
        {
            InitializeComponent();
            Loaded += delegate
            {
                this.Size = this.PositionCanvas.ActualHeight / 7.0;
                this.HeightMargin = this.PositionCanvas.ActualHeight / 2.0 - this.Size / 2.0;
                CalculateControlWidth();
            };
        }

        private void Redraw()
        {
            this.PositionCanvas.Children.Clear();
            double width = this.Left;
            for (int i = 0; i < this.Total; i++ )
            {
                if (i == this.CurrentPosition)
                {
                    Rectangle rect = new Rectangle();
                    rect.Height = this.Size;
                    rect.Width = this.Size * 3;
                    rect.Fill = new SolidColorBrush(Colors.White);
                    this.PositionCanvas.Children.Add(rect);
                    Canvas.SetLeft(rect, width);
                    Canvas.SetTop(rect, this.HeightMargin);
                    width += this.Size * 4;
                }
                else 
                {
                    Ellipse newEllipse = new Ellipse();
                    newEllipse.Width = this.Size;
                    newEllipse.Height = this.Size;
                    newEllipse.Fill = new SolidColorBrush(Colors.White);
                    this.PositionCanvas.Children.Add(newEllipse);
                    newEllipse.SetValue(Canvas.LeftProperty, width);
                    newEllipse.SetValue(Canvas.TopProperty, this.HeightMargin);
                    width += this.Size * 2;
                }
               
            }
            width += this.Size;
            //this.PositionCanvas.Width = width;
        }
    }
}
