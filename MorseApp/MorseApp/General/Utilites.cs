﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MorseApp.General
{
    public class Utilites
    {

        public bool IsDouble(string text, out double result)
        {
            Regex regex = new Regex("^[0-9]{1,3}([.,][0-9]{1,3})?$");
            result = -1;
            if (regex.IsMatch(text))
            {
                text = text.Replace(',', '.');
                result = double.Parse(text, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo);
                return true;
            }
            else
                return false;
            
            
        }
    }
}
