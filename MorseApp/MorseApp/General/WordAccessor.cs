﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Storage;

namespace MorseApp.General
{

    public sealed class WordAccessor
    {
        private static volatile WordAccessor instance;
        private static object syncRoot = new Object();

        private List<string> rusWords = new List<string>();
        private List<string> engWords = new List<string>();

        private Random random = new Random();
        private bool isLoaded = false;
        private WordAccessor() 
        {}
        async private void  LoadTextFiles()
        {

            string fileContent;
            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri(@"ms-appx:///Assets//eng.txt"));
            using (StreamReader sRead = new StreamReader(await file.OpenStreamForReadAsync()))
            {
                fileContent = await sRead.ReadToEndAsync();
                this.engWords = fileContent.Split('\n').Select(s => s.Trim()).ToList();
            }

            file = await StorageFile.GetFileFromApplicationUriAsync(new Uri(@"ms-appx:///Assets//rus.txt"));
            using (StreamReader sRead = new StreamReader(await file.OpenStreamForReadAsync()))
            {
                fileContent = await sRead.ReadToEndAsync();
                this.rusWords = fileContent.Split('\n').Select(s => s.Trim()).ToList();
            }
            this.isLoaded = true;
        }

        public static WordAccessor Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new WordAccessor();
                            instance.LoadTextFiles();
                        }
                            
                    }
                }
                return instance;
            }
        }
        public string GetRandomWord(TextCodec.LOCALE locale)
        {
            if (!this.isLoaded)
            {
                if (Settings.Instance.Locale == TextCodec.LOCALE.RUS)
                {
                    return "ПОПРОБУЙ";
                }
                else
                {
                    return "MORSEAPP";
                }
            }
                

            if (locale == TextCodec.LOCALE.INT && this.engWords.Count > 0)
            {
                return this.engWords[this.random.Next(this.engWords.Count)].ToUpper().Trim();
            }else if (locale == TextCodec.LOCALE.RUS && this.rusWords.Count > 0)
            {
                return this.rusWords[this.random.Next(this.rusWords.Count)].ToUpper().Trim();
            }
            return "";
        }
    }
}
