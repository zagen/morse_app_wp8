﻿using GoogleAds;
using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace MorseApp.General
{
    public static class Extensions
    {
        private static Storyboard Storyboard = null;
        public static void ShowPopup(this TextBlock text, string content)
        {
            if (!Settings.Instance.IsInformationMessagesShowing)
                return;

            text.Text = content;
            if(Extensions.Storyboard != null)
            {
                Extensions.Storyboard.SkipToFill();
                Extensions.Storyboard = null;
            }
            double animationDuration = 0.5;
            double showingDuration = 1.0;
            Duration duration = new Duration(TimeSpan.FromSeconds(showingDuration));

            // Create two DoubleAnimations and set their properties.
            DoubleAnimationUsingKeyFrames fadeAnimation = new DoubleAnimationUsingKeyFrames();
            fadeAnimation.Duration = duration;
            DoubleKeyFrame fadeKeyFrame = new LinearDoubleKeyFrame();
            
            fadeKeyFrame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(animationDuration));
            fadeKeyFrame.Value = 1;

            DoubleKeyFrame delayKeyFrame = new LinearDoubleKeyFrame();
            delayKeyFrame.KeyTime = KeyTime.FromTimeSpan(TimeSpan.FromSeconds(showingDuration));
            delayKeyFrame.Value = 1;

            fadeAnimation.KeyFrames.Add(fadeKeyFrame);
            fadeAnimation.KeyFrames.Add(delayKeyFrame);


            Storyboard sb = new Storyboard();
            sb.AutoReverse = true;
            sb.Duration = duration;

            sb.Children.Add(fadeAnimation);

            Storyboard.SetTarget(fadeAnimation, text);

            Storyboard.SetTargetProperty(fadeAnimation, new PropertyPath("(Opacity)"));
            
            sb.Begin();
            Extensions.Storyboard = sb;
        }
        private static int CompareStringsWithAddingInInlineCollection(InlineCollection collection, string correct, string inputed, SolidColorBrush CorrectSolidBrush, SolidColorBrush IncorrectSolidBrush )
        {

            inputed = inputed.ToUpper();
            inputed = inputed.Replace("Ё", "E");

            if (correct.Equals(inputed))
            {
                Run r = new Run();
                r.Text = correct;
                r.FontWeight = FontWeights.Bold;
                r.FontSize = 35;
                r.Foreground = CorrectSolidBrush;
                collection.Add(r);
                return 0;
            }
            else
            {
                int minLength = (correct.Length < inputed.Length) ? correct.Length : inputed.Length;

                for (int i = 0; i < minLength; i++)
                {
                    Run r = new Run();
                    r.Text = correct[i].ToString();
                    r.FontWeight = FontWeights.Bold;
                    r.FontSize = 35;
                    if (correct[i] == inputed[i])
                    {
                        r.Foreground = CorrectSolidBrush;

                    }
                    else
                    {
                        r.Foreground = IncorrectSolidBrush;
                    }
                    collection.Add(r);
                }
                if (correct.Length > inputed.Length)
                {
                    string rest = correct.Substring(inputed.Length, correct.Length - inputed.Length);
                    Run r = new Run();
                    r.Text = rest;
                    r.FontWeight = FontWeights.Bold;
                    r.FontSize = 35;
                    r.Foreground = IncorrectSolidBrush;
                    collection.Add(r);
                    return 1;
                }
                else if (correct.Length == inputed.Length)
                    return 2;
                else
                    return -1;
            }

        }

        public static int CompareTextBlockWithStringClearInlines(this TextBlock textBlock, String textToCompareTo, SolidColorBrush CorrectSolidBrush = null, SolidColorBrush IncorrectSolidBrush = null)
        {
            if (CorrectSolidBrush == null)
            {
                CorrectSolidBrush = new SolidColorBrush(Colors.Black);
            }
            if (IncorrectSolidBrush == null)
            {
                IncorrectSolidBrush = new SolidColorBrush(Colors.Gray);
            }
            InlineCollection inlines = textBlock.Inlines;
            String textBlockText = textBlock.Text;
            inlines.Clear();
            return CompareStringsWithAddingInInlineCollection(inlines, textBlockText, textToCompareTo, CorrectSolidBrush, IncorrectSolidBrush);
           
            
        }
        public static int CompareTextBlockWithStringWithoutClearInlines(this TextBlock textBlock,string correct , string inputed, SolidColorBrush CorrectSolidBrush = null, SolidColorBrush IncorrectSolidBrush = null)
        {
            if (CorrectSolidBrush == null)
            {
                CorrectSolidBrush = new SolidColorBrush(Colors.White);
            }
            if (IncorrectSolidBrush == null)
            {
                IncorrectSolidBrush = new SolidColorBrush(Colors.Gray);
            }
            InlineCollection inlines = textBlock.Inlines;
            
            return CompareStringsWithAddingInInlineCollection(inlines, correct, inputed, CorrectSolidBrush, IncorrectSolidBrush);


        }
    }
    public static class PhoneApplicationPageExtensions
    {
        public static void AddAds(this PhoneApplicationPage page, Grid LayoutRoot, int Row)
        {
            if (Settings.Instance.AdsRemoved)
                return;
            AdView bannerAd = new AdView
            {
                Format = AdFormats.SmartBanner,
                AdUnitID = "ca-app-pub-4530434209938102/2526714874"
            };

            bannerAd.VerticalAlignment = VerticalAlignment.Bottom;

            LayoutRoot.Children.Add(bannerAd);

            AdRequest adRequest = new AdRequest();
            //adRequest.ForceTesting = true;
            bannerAd.LoadAd(adRequest);
            Grid.SetRow(bannerAd, Row);
        }
    }
}
