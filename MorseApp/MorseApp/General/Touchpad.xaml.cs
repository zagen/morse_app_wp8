﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.Diagnostics;
using Microsoft.Xna.Framework.Audio;
using System.Windows.Threading;
using Microsoft.Xna.Framework;
using System.ComponentModel;
using DynamicSoundDemo;
using MorseApp.General;
using System.Threading;

namespace MorseApp
{
    public partial class TouchPad : UserControl
    {
        public const char LONG_SPACE = '^';
        private SolidColorBrush WhiteBrush = new SolidColorBrush(Colors.White);
        private SolidColorBrush TransparentBrush = new SolidColorBrush(Colors.Transparent);
        private SoundManager sm = new SoundManager();

        private Timer timer;
        private double SignalDuration;
        private bool IsTouchDown = false;
        private DateTime touchBegin;
        public delegate void MorseCodeInputedDelegate(char code);
        public MorseCodeInputedDelegate MorseCodeInputed { get; set; }

        
        public void SetEnabled(bool IsEnabled)
        {
            if (IsEnabled)
            {
                this.IsEnabled = true;
            }
            else
            {
                this.IsEnabled = false;
                this.sm.Stop();
                if (timer != null)
                {
                    this.timer.Dispose();
                    this.timer = null;
                }
                

            }
        }
        public TouchPad()
        {
            InitializeComponent();
            
            Loaded += delegate
            {
                this.SignalDuration = Settings.Instance.SignalDurationMS;
                double size = (this.ActualHeight > this.ActualWidth) ? this.ActualWidth : this.ActualHeight;
                Circle.Width = size;
                Circle.Height = size;
                Circle.Opacity = 1;
                
            };
            
        }
        private void Tap_LeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!IsEnabled)
                return;
            IsTouchDown = true;
            if (this.timer != null)
            {
                this.timer.Dispose();
                this.timer = null;
            }
            this.touchBegin = DateTime.Now;
            Circle.Fill = WhiteBrush;
            sm.Play();
        }

        private void TouchPad_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if(IsEnabled)
                TouchEnded();
            IsTouchDown = false;
        }

        private void TouchEnded()
        {
            if (!IsTouchDown)
            {
                return;
            }
            DateTime touchEnd = DateTime.Now;
            double timeDiff = (touchEnd - touchBegin).TotalMilliseconds;

            
            if (timeDiff < 3.0 * SignalDuration)
            {
                
                if(MorseCodeInputed != null)
                    MorseCodeInputed('.');
            }
            else
            {
                if(MorseCodeInputed != null)
                    MorseCodeInputed('-');
            }
            Circle.Fill = TransparentBrush;
            sm.Stop();
            this.timer = new Timer(delegate
            {
                if (MorseCodeInputed != null)
                    Dispatcher.BeginInvoke(delegate
                    {
                        MorseCodeInputed(' ');
                    });
                this.timer = new Timer(delegate
                {
                    if (MorseCodeInputed != null)
                        Dispatcher.BeginInvoke(delegate
                        {
                            MorseCodeInputed(LONG_SPACE);
                        });
                    this.timer = null;
                }, null, (int)SignalDuration * 4, Timeout.Infinite);
            }, null, (int)SignalDuration * 3, Timeout.Infinite);
        }
        public void Destroy()
        {
            sm.Stop();
        }
    }

   
}
