﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.IsolatedStorage;

namespace MorseApp.General
{
    public class Settings
    {
        public const string ADS_REMOVED_DEFAULT = "";
        public const int TRANSMITTER_DEFAULT = 0;
        public const int LOCALE_DEFAULT = 0;
        public const bool IS_MNEMONICS_SHOWED_DEFAULT = true;
        public const bool SHOW_INFORMATION_MESSAGES_DEFAULT = true;
        public const int SIGNAL_DURATION_DEFAULT = 15;
        public const int BRIGHTNESS_DEFAULT = 100;
        public const int GENERATOR_LENGTH_DEFAULT = 2;
        public const int GROUPS_ON_RECEIVE_DEFAULT = 1;
        public const int FREQUENCE_DEFAULT = 1000;
        public const string INT_ABC_DEFAULT = "abcdefghijklmnopqrstuvwxyz";
        public const string RUS_ABC_DEFAULT = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        public const string ACHIEVEMENTS_DEFAULT = "[]";
        public const int PERSONAL_RECORD_DEFAULT = 0;
        public const int SEQUENSE_TYPE_DEFAULT = 0;
        public const string FILENAME_DEFAULT = "";
        public const int TEXT_FILE_POSITION_DEFAULT = 0;

        public const string ADS_REMOVED_NAME = "ads_removed_setting";
        public const string TRANSMITTER_NAME = "transmitter_setting";
        public const string LOCALE_NAME = "locale_setting";
        public const string IS_MNEMONICS_SHOWED_NAME = "is_mnemonic_showed_setting";
        public const string SHOW_INFORMATION_MESSAGES_NAME  = "show_information_messages_setting";
        public const string SIGNAL_DURATION_NAME = "signal_duration_setting";
        public const string BRIGHTNESS_NAME = "brightness_setting";
        public const string GENERATOR_LENGTH_NAME = "generator_length_setting";
        public const string GROUPS_ON_RECEIVE_NAME = "groups_on_receive_setting";
        public const string FREQUENCE_NAME = "frequence_setting";
        public const string INT_ABC_NAME = "int_abc_setting";
        public const string RUS_ABC_NAME = "rus_abc_setting";
        public const string ACHIEVEMENTS_NAME = "achieved_achievements_setting";
        public const string PERSONAL_RECORD_NAME = "personal_record_setting";
        public const string SEQUENSE_TYPE_NAME = "sequence_type_setting";
        public const string FILENAME_NAME = "filename_setting";
        public const string TEXT_FILE_POSITION_NAME = "text_file_position_setting";

        public event PropertyChangedEventHandler PropertyChanged;

        private string mAdsRemoved;
        private int mLocale;
        private int mTransmitter;
        private bool mIsMnemonicsShowed;
        private bool mShowInformationMessages;
        private int mSignalDuration;
        private int mBrightness;
        private int mGeneratorLength;
        private int mGroupsOnReceive;
        private int mFrequence;
        private string mIntAbc;
        private string mRusAbc;
        private List<string> mAchievedAchievements;
        private int mPersonalRecord;
        private int mSequenceType;
        private string mFilename;
        private int mTextFilePosition;
        
        private static volatile Settings instance;
        private static object syncRoot = new Object();

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        public bool AdsRemoved
        {
            get
            {
                return mAdsRemoved.Length != 0; //IMPORTANT! BILLING
            }
            set
            {
                if (value)
                {
                    mAdsRemoved = "Make some money...";
                    Sync();
                    OnPropertyChanged("AdsRemoved");
                }
                else
                {
                    mAdsRemoved = "";
                }
                    
            }

        }
        public TextCodec.LOCALE Locale
        {
            get
            {
                return (mLocale == 1) ? TextCodec.LOCALE.RUS : TextCodec.LOCALE.INT;
            }
            set
            {
                if (value == TextCodec.LOCALE.RUS)
                    mLocale = 1;
                else
                    mLocale = 0;
                OnPropertyChanged("Locale");
            }
        }
        public int Transmitter
        {
            get
            {
                return mTransmitter;
            }
            set
            {
                mTransmitter = value;
                OnPropertyChanged("Transmitter");
            }
        }
        public bool IsMnemonicsShowed
        {
            get
            {
                return mIsMnemonicsShowed;
            }
            set
            {
                mIsMnemonicsShowed = value;
                OnPropertyChanged("IsMnemonicsShowed");
            }
        }

        public bool IsInformationMessagesShowing
        {
            get
            {
                return mShowInformationMessages;
            }
            set
            {
                mShowInformationMessages = value;
                OnPropertyChanged("IsInformationMessagesShowing");
            }
        }
        public int SignalDuration
        {
            get
            {
                return mSignalDuration;
            }
            set
            {
                mSignalDuration = value;
                OnPropertyChanged("SignalDuration");
                OnPropertyChanged("SignalDurationMS");
            }
        }
        public double SignalDurationMS
        {
            get
            {
                return Math.Round(1200d/this.mSignalDuration);
            }
        }
        public int Brightness
        {
            get
            {
                return mBrightness;
            }
            set
            {
                mBrightness = value;
                OnPropertyChanged("Brightness");
            }
        }
        public int GeneratorLength
        {
            get
            {
                return mGeneratorLength;
            }
            set
            {
                mGeneratorLength = value;
                OnPropertyChanged("GeneratorLength");
            }
        }
        public int GroupOnReceive
        {
            get
            {
                return mGroupsOnReceive;
            }
            set
            {
                mGroupsOnReceive = value;
                OnPropertyChanged("GroupOnReceive");
            }
        }

        public int Frequence
        {
            get
            {
                return mFrequence;
            }
            set
            {
                mFrequence = value;
                OnPropertyChanged("Frequence");
            }
        }

        public string IntAbc
        {
            get
            {
                return mIntAbc;
            }
            set
            {
                mIntAbc = value;
                OnPropertyChanged("IntAbc");
            }
        }

        public string RusAbc
        {
            get
            {
                return mRusAbc;
            }
            set
            {
                mRusAbc = value;
                OnPropertyChanged("RusAbc");
            }
        }

        public List<string> Achievements
        {
            get
            {
                return mAchievedAchievements;
            }
            set
            {
                mAchievedAchievements = value;
                OnPropertyChanged("Achievements");
            }
        }

        //property for personal record
        public int PersonalRecord
        {
            get
            {
                return mPersonalRecord;
            }
            set
            {
                mPersonalRecord = value;
                OnPropertyChanged("PersonalRecord");
            }
        }

        public Generator.SequenceType SequenceType
        {
            get
            {
                return (mSequenceType == 0) ? Generator.SequenceType.CHARS : Generator.SequenceType.WORDS;
            }
            set
            {
                if (value == Generator.SequenceType.CHARS)
                    mSequenceType = 0;
                else
                    mSequenceType = 1;
                OnPropertyChanged("SequenceType");
            }
        }

        public string Filename
        {
            get
            {
                return mFilename;
            }
            set
            {
                mFilename = value;
                OnPropertyChanged("Filename");
            }
        }
        public int TextFilePosition
        {
            get
            {
                return mTextFilePosition;
            }
            set
            {
                mTextFilePosition = value;
                OnPropertyChanged("TextFilePosition");
            }
        }

        private IsolatedStorageSettings settings;
        
        private Settings()
        {
            this.settings = IsolatedStorageSettings.ApplicationSettings;
        }
        public static Settings Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new Settings();
                            instance.Load();
                        }

                    }
                }
                return instance;
            }
        }
        public void Load()
        {
            if (!settings.Contains(ADS_REMOVED_NAME))
            {
                this.mAdsRemoved = "";
            }
            else
            {
                this.mAdsRemoved = settings[ADS_REMOVED_NAME] as string;
            }

            if (!settings.Contains(TRANSMITTER_NAME))
            {
                this.mTransmitter = TRANSMITTER_DEFAULT;
            }
            else
            {
                this.mTransmitter = (int)settings[TRANSMITTER_NAME];
            }

            //locale
            if (!settings.Contains(LOCALE_NAME))
            {
                this.mLocale = LOCALE_DEFAULT;
            }
            else
            {
                this.mLocale = (int)settings[LOCALE_NAME];
            }

            //show mnemonics in handbook
            if (!settings.Contains(IS_MNEMONICS_SHOWED_NAME))
            {
                this.mIsMnemonicsShowed = IS_MNEMONICS_SHOWED_DEFAULT;
            }
            else
            {
                
                bool.TryParse(settings[IS_MNEMONICS_SHOWED_NAME] as string, out this.mIsMnemonicsShowed);
            }

            //show pop-up info
            if (!settings.Contains(SHOW_INFORMATION_MESSAGES_NAME))
            {
                this.mShowInformationMessages = SHOW_INFORMATION_MESSAGES_DEFAULT;
            }
            else
            {
                bool.TryParse(settings[SHOW_INFORMATION_MESSAGES_NAME] as string, out this.mShowInformationMessages);
            }

            //transmission speed
            if (!settings.Contains(SIGNAL_DURATION_NAME))
            {
                this.mSignalDuration = SIGNAL_DURATION_DEFAULT;
            }
            else
            {
                this.mSignalDuration = (int)settings[SIGNAL_DURATION_NAME];
            }

            //brightness of screen transmitter
            if (!settings.Contains(BRIGHTNESS_NAME))
            {
                this.mBrightness = BRIGHTNESS_DEFAULT;
            }
            else
            {
                this.mBrightness = (int)settings[BRIGHTNESS_NAME];
            }

            //length of generated sequence
            if (!settings.Contains(GENERATOR_LENGTH_NAME))
            {
                this.mGeneratorLength = GENERATOR_LENGTH_DEFAULT;
            }
            else
            {
                this.mGeneratorLength = (int)settings[GENERATOR_LENGTH_NAME];
            }


            if (!settings.Contains(GROUPS_ON_RECEIVE_NAME))
            {
                this.mGroupsOnReceive = GROUPS_ON_RECEIVE_DEFAULT;
            }
            else
            {
                this.mGroupsOnReceive = (int)settings[GROUPS_ON_RECEIVE_NAME];
            }

            //frequence
            if (!settings.Contains(FREQUENCE_NAME))
            {
                this.mFrequence = FREQUENCE_DEFAULT;
            }
            else
            {
                this.mFrequence = (int)settings[FREQUENCE_NAME];
            }

            //latin abc for generating sequence
            if (!settings.Contains(INT_ABC_NAME))
            {
                this.mIntAbc = INT_ABC_DEFAULT;
            }
            else
            {
                this.mIntAbc = settings[INT_ABC_NAME] as string;
            }

            //rus abc for generating sequence
            if (!settings.Contains(RUS_ABC_NAME))
            {
                this.mRusAbc = RUS_ABC_DEFAULT;
            }
            else
            {
                this.mRusAbc = settings[RUS_ABC_NAME] as string;
            }

            //achieved achievements
            if (!settings.Contains(ACHIEVEMENTS_NAME))
            {
                this.mAchievedAchievements = JsonConvert.DeserializeObject<List<string>>(ACHIEVEMENTS_DEFAULT);
            }
            else
            {
                this.mAchievedAchievements = JsonConvert.DeserializeObject<List<string>>(settings[ACHIEVEMENTS_NAME] as string);
            }

            //personal record
            if (!settings.Contains(PERSONAL_RECORD_NAME))
            {
                this.mPersonalRecord = PERSONAL_RECORD_DEFAULT;
            }
            else
            {
                this.mPersonalRecord = (int)settings[PERSONAL_RECORD_NAME];
            }

            //generated sequence type
            if (!settings.Contains(SEQUENSE_TYPE_NAME))
            {
                this.mSequenceType = SEQUENSE_TYPE_DEFAULT;
            }
            else
            {
                this.mSequenceType = (int)settings[SEQUENSE_TYPE_NAME];
            }

            //string for text filename
            if (!settings.Contains(FILENAME_NAME))
            {
                this.mFilename = FILENAME_DEFAULT;
            }
            else
            {
                this.mFilename = settings[FILENAME_NAME] as string;
            }


            //position in text file
            if (!settings.Contains(TEXT_FILE_POSITION_NAME))
            {
                this.mTextFilePosition = TEXT_FILE_POSITION_DEFAULT;
            }
            else
            {
                this.mTextFilePosition = (int)settings[TEXT_FILE_POSITION_NAME];
            }

        }
        public void Sync()
        {
            //ads
            if (!settings.Contains(ADS_REMOVED_NAME))
            {
                settings.Add(ADS_REMOVED_NAME, this.mAdsRemoved);
            }
            else
            {
                settings[ADS_REMOVED_NAME] = this.mAdsRemoved;
            }

            //transmitter
            if (!settings.Contains(TRANSMITTER_NAME))
            {
                settings.Add(TRANSMITTER_NAME, this.mTransmitter);
            }
            else
            {
                settings[TRANSMITTER_NAME] = this.mTransmitter;
            }

            //locale
            if (!settings.Contains(LOCALE_NAME))
            {
                settings.Add(LOCALE_NAME, this.mLocale);
            }
            else
            {
                settings[LOCALE_NAME] = this.mLocale;
            }

            //show mnemonics in handbook
            if (!settings.Contains(IS_MNEMONICS_SHOWED_NAME))
            {
                settings.Add(IS_MNEMONICS_SHOWED_NAME, this.mIsMnemonicsShowed.ToString());
            }
            else
            {
                settings[IS_MNEMONICS_SHOWED_NAME] = this.mIsMnemonicsShowed.ToString();
            }

            //show pop-up info
            if (!settings.Contains(SHOW_INFORMATION_MESSAGES_NAME))
            {
                settings.Add(SHOW_INFORMATION_MESSAGES_NAME, this.mShowInformationMessages.ToString());
            }
            else
            {
                
                settings[SHOW_INFORMATION_MESSAGES_NAME] = this.mShowInformationMessages.ToString();
            }

            //transmission speed
            if (!settings.Contains(SIGNAL_DURATION_NAME))
            {
                settings.Add(SIGNAL_DURATION_NAME, this.mSignalDuration);
            }
            else
            {
                settings[SIGNAL_DURATION_NAME] = this.mSignalDuration;
            }

            //brightness of screen transmitter
            if (!settings.Contains(BRIGHTNESS_NAME))
            {
                settings.Add(BRIGHTNESS_NAME, this.mBrightness);
            }
            else
            {
                settings[BRIGHTNESS_NAME] = this.mBrightness;
            }

            //length of generated sequence
            if (!settings.Contains(GENERATOR_LENGTH_NAME))
            {
                settings.Add(GENERATOR_LENGTH_NAME, this.mGeneratorLength);
            }
            else
            {
                settings[GENERATOR_LENGTH_NAME] = this.mGeneratorLength;
            }


            if (!settings.Contains(GROUPS_ON_RECEIVE_NAME))
            {
                settings.Add(GROUPS_ON_RECEIVE_NAME, this.mGroupsOnReceive);
            }
            else
            {
                settings[GROUPS_ON_RECEIVE_NAME] = this.mGroupsOnReceive;
            }

            //frequence
            if (!settings.Contains(FREQUENCE_NAME))
            {
                settings.Add(FREQUENCE_NAME, this.mFrequence);
            }
            else
            {
                settings[FREQUENCE_NAME] = this.mFrequence;
            }

            //latin abc for generating sequence
            if (!settings.Contains(INT_ABC_NAME))
            {
                settings.Add(INT_ABC_NAME, this.mIntAbc);
            }
            else
            {
                settings[INT_ABC_NAME] = this.mIntAbc;
            }

            //rus abc for generating sequence
            if (!settings.Contains(RUS_ABC_NAME))
            {
                settings.Add(RUS_ABC_NAME, this.mRusAbc);
            }
            else
            {
                settings[RUS_ABC_NAME] = this.mRusAbc;
            }
            //achieved achievements
            if (!settings.Contains(ACHIEVEMENTS_NAME))
            {
                settings.Add(ACHIEVEMENTS_NAME, JsonConvert.SerializeObject(this.mAchievedAchievements));
            }
            else
            {
                settings[ACHIEVEMENTS_NAME] = JsonConvert.SerializeObject(this.mAchievedAchievements);
            }

            //personal best
            if (!settings.Contains(PERSONAL_RECORD_NAME))
            {
                settings.Add(PERSONAL_RECORD_NAME, this.mPersonalRecord);
            }
            else
            {
                settings[PERSONAL_RECORD_NAME] = this.mPersonalRecord;
            }

            //sequence type
            if (!settings.Contains(SEQUENSE_TYPE_NAME))
            {
                settings.Add(SEQUENSE_TYPE_NAME, this.mSequenceType);
            }
            else
            {
                settings[SEQUENSE_TYPE_NAME] = this.mSequenceType;
            }
            //text file name
            if (!settings.Contains(FILENAME_NAME))
            {
                settings.Add(FILENAME_NAME, this.mFilename);
            }
            else
            {
                settings[FILENAME_NAME] = this.mFilename;
            }
            settings.Save();

            //position in text file
            if (!settings.Contains(TEXT_FILE_POSITION_NAME))
            {
                settings.Add(TEXT_FILE_POSITION_NAME, this.mTextFilePosition);
            }
            else
            {
                settings[TEXT_FILE_POSITION_NAME] = this.mTextFilePosition;
            }
        }
        public void ResetDefaults()
        {
            this.mLocale = LOCALE_DEFAULT;
            this.mTransmitter = TRANSMITTER_DEFAULT;
            this.mIsMnemonicsShowed = IS_MNEMONICS_SHOWED_DEFAULT;
            this.mShowInformationMessages = SHOW_INFORMATION_MESSAGES_DEFAULT;
            this.mSignalDuration = SIGNAL_DURATION_DEFAULT;
            this.mBrightness = BRIGHTNESS_DEFAULT;
            this.mGeneratorLength = GENERATOR_LENGTH_DEFAULT;
            this.mGroupsOnReceive = GROUPS_ON_RECEIVE_DEFAULT;
            this.mFrequence = FREQUENCE_DEFAULT;
            this.mIntAbc = INT_ABC_DEFAULT;
            this.mRusAbc = RUS_ABC_DEFAULT;
            this.mAchievedAchievements = JsonConvert.DeserializeObject<List<string>>(ACHIEVEMENTS_DEFAULT);
            this.mPersonalRecord = PERSONAL_RECORD_DEFAULT;
            this.mSequenceType = SEQUENSE_TYPE_DEFAULT;
            this.mFilename = FILENAME_DEFAULT;
            this.mTextFilePosition = TEXT_FILE_POSITION_DEFAULT;
            this.mAdsRemoved = "";
            this.Sync();
        }
    }
}
