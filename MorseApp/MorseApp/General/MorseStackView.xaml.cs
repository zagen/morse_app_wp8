﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Diagnostics;


namespace MorseApp
{
    public partial class MorseStackView : UserControl
    {

        public static readonly DependencyProperty MorseCodeProperty =
            DependencyProperty.Register("MorseCode", typeof(string), typeof(MorseStackView), new PropertyMetadata(default(string), MorseCodePropertyChanged));
        

        private List<char> symbols = new List<char>();
        private double dotSize = 5;
        private double topMargin = 5;
        
        public string MorseCode
        {
            get{
                return string.Join("", this.symbols.ToArray()).Trim();
            }
            set
            {
                Clear();
                Push(value);
            }
        }

        private static void MorseCodePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MorseStackView morseView = d as MorseStackView;
            morseView.Clear();
            morseView.Push(e.NewValue as string);
        }
    
        public void Push(char morseSymbol)
        {
            bool isSpaceSymbol = (morseSymbol != '.') && (morseSymbol != '-');
            bool isLastAddedSymbolSpace = false;

            if (this.symbols.Count != 0)
            {
                char lastSymbol = this.symbols.Last();
                isLastAddedSymbolSpace = (lastSymbol != '.') && (lastSymbol != '-');
            }
            
            if ( !(isSpaceSymbol &&  isLastAddedSymbolSpace))
            {
                this.symbols.Add(morseSymbol);
            }
            this.Redraw();
        }

        public void Push(string morseSymbols)
        {
            foreach (char symbol in morseSymbols)
            {
                this.symbols.Add(symbol);
            }
            this.Redraw();
        }

        public void Clear()
        {
            this.symbols.Clear();
            this.Redraw();
        }
        public MorseStackView()
        {
            InitializeComponent();

            Loaded += delegate
            {
                this.dotSize = this.stackCanvas.ActualHeight / 5.0;
                this.topMargin = this.stackCanvas.ActualHeight / 2.0 - this.dotSize / 2.0;
                Redraw();
            };
        }
        private void Redraw()
        {
            this.stackCanvas.Children.Clear();
            double width = this.dotSize;
            foreach(char symbol in this.symbols)
            {
                if(symbol == '-')
                {
                    Rectangle rect = new Rectangle();
                    rect.Height = this.dotSize;
                    rect.Width = this.dotSize * 3;
                    rect.Fill = new SolidColorBrush(Colors.White);
                    this.stackCanvas.Children.Add(rect);
                    Canvas.SetLeft(rect, width);
                    Canvas.SetTop(rect, this.topMargin);
                    width += this.dotSize * 4;
                }else if(symbol == '.')
                {
                    Ellipse newEllipse = new Ellipse();
                    newEllipse.Width = this.dotSize;
                    newEllipse.Height = this.dotSize;
                    newEllipse.Fill = new SolidColorBrush(Colors.White);
                    this.stackCanvas.Children.Add(newEllipse);
                    newEllipse.SetValue(Canvas.LeftProperty, width);
                    newEllipse.SetValue(Canvas.TopProperty, this.topMargin);
                    width += this.dotSize * 2;

                }else
                {
                    width += this.dotSize * 3;
                } 
            }
            width += this.dotSize;
            this.stackCanvas.Width = width ;
            this.scrollViewer.ScrollToHorizontalOffset(width);
        }
    }
}
