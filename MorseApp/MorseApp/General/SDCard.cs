﻿using Microsoft.Phone.Storage;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MorseApp.General
{
    public class SDCard
    {

        private ExternalStorageFolder _CurrentPath;
        private string FileExtension = ".text";
        public ExternalStorageFolder CurrentPath 
        {
            get
            {
                return _CurrentPath;
            }
        }
        private ExternalStorageFolder _SDCardRoot;
        private ExternalStorageDevice _SDCard;

        private bool _IsSDCardExist = false;

        public async Task GetSDCard()
        {
            _SDCard = (await ExternalStorage.GetExternalStorageDevicesAsync()).FirstOrDefault();
            if (_SDCard != null)
            {
                ExternalStorageFolder sdrootFolder = _SDCard.RootFolder;

                if (sdrootFolder != null)
                {
                    this._SDCardRoot = sdrootFolder;
                    this._CurrentPath = this._SDCardRoot;
                    this._IsSDCardExist = true;
                }
                else
                {
                    MessageBox.Show("Failed to get root folder on SD card");
                }
            }
            else
            {
                MessageBox.Show("SD Card not found on device");
            }



        }
        public async Task<IEnumerable<ExternalStorageFile>> GetFiles()
        {
            if (_IsSDCardExist)
            {
                return await _CurrentPath.GetFilesAsync();
            }
            return null;

        }

        public async Task<IEnumerable<ExternalStorageFolder>> GetFolders()
        {
            if (_IsSDCardExist)
            {
                return await _CurrentPath.GetFoldersAsync();
            }
            return null;
        }
        public async Task ToParent()
        {
            if (_CurrentPath.Path.Length > 0) //check if it not root folder
            {
                string path = _CurrentPath.Path;
                int indexOfSlash = path.LastIndexOf('\\');
                if (indexOfSlash == -1)
                {
                    _CurrentPath = _SDCardRoot;
                }
                else
                {
                    path = path.Substring(0, indexOfSlash);
                    _CurrentPath = await _SDCardRoot.GetFolderAsync(path);
                }
            }
        }
        public async Task OpenDir(string dirname)
        {
            if (dirname.Length == 0)
            {
                _CurrentPath = _SDCardRoot;
            }else if (dirname.Equals("..")) //get to parent dir if it available
            {
                await ToParent();
            }
            else
            {
                string delim = "";
                if (_CurrentPath.Path.Length > 0)
                    delim = "\\";
                string selectedFullPath = _CurrentPath.Path + delim + dirname;
                _CurrentPath = await _SDCardRoot.GetFolderAsync(selectedFullPath);
            }
        }
        public async Task<ExternalStorageFile> GetFileByFullPath(string fullpath)
        {
            string path = GetPathFromFullPath(fullpath);
            string name = GetFilenameFromFullPath(fullpath);
            await OpenDir(path);
            var files = await GetFiles();

            foreach (var file in files)
            {
                if (file.Name.Equals(name))
                {
                    return file;
                }
            }
            return null;
        }

        public async Task<string> ReadAllFile(ExternalStorageFile file)
        {
            string buffer = "";
            try 
            {
                Stream s = await file.OpenForReadAsync();
                if (s != null || s.Length == 0)
                {
                    long streamLength = s.Length;
                    Ude.CharsetDetector cdet = new Ude.CharsetDetector();
                    cdet.Feed(s);
                    cdet.DataEnd();
                    if (cdet.Charset != null) {
                        s.Seek(0, 0);
                        
                        if (cdet.Charset.Equals("windows-1251"))
                        {
                            StreamReader sr = new StreamReader(s, new Windows1251Encoding());
                            buffer = sr.ReadToEnd();
                        }
                        else if (cdet.Charset.Equals("UTF-8"))
                        {
                            StreamReader sr = new StreamReader(s);
                            buffer = sr.ReadToEnd();
                        }
                        else
                            buffer = "";

                    } else {
                        buffer = "";
                    }
                    
                }
            }catch
            {
                buffer = "";
            }

            return buffer;

        }
        public string GetPathFromFullPath(string fullpath)
        {
            if (fullpath == ".." || fullpath == ".")
            {
                return fullpath;
            }

            int indexOfSlash = fullpath.LastIndexOf('\\');
            string path;
            
            if (indexOfSlash == -1)
            {
                if (fullpath.EndsWith(FileExtension))
                    return "";
                else
                    return fullpath;
            }
            else
            {
                path = fullpath.Substring(0, indexOfSlash);
                return path;
            }
        }
        public string GetFilenameFromFullPath(string fullpath)
        {
            if (fullpath == ".." || fullpath == ".")
            {
                return "";
            }

            int indexOfSlash = fullpath.LastIndexOf('\\');
            string path;
            if (indexOfSlash == -1)
            {
                if (fullpath.EndsWith(FileExtension))
                    return fullpath;
                else
                    return "";
            }
            else
            {
                path = fullpath.Substring(indexOfSlash + 1);
                return path;
            }
        }

    }


}
