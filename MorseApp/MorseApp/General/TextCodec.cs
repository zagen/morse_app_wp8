﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorseApp.General
{
    public class TextCodec
    {
        public enum LOCALE { ALL = 2, RUS = 1, INT = 0 };
        public LOCALE Locale { get; set; }
        public LOCALE localeByDefault { get; set; }

        private string decodedText;
        private string encodedText;

        private Dictionary<char, string> encMapIntLet;

        private Dictionary<string, string> decMapIntLet ;

        private  Dictionary<char, string> encMapRusLet;
        private Dictionary<string, string> decMapRusLet;

        private Dictionary<char, string> encMapSymbols;
        private Dictionary<string, string> decMapSymbols;
        public TextCodec(){
            Settings settings = Settings.Instance;

            this.localeByDefault = settings.Locale;
            this.Locale = settings.Locale;

            this.encMapIntLet = new Dictionary<char, string>()
                                    {
                                        {'a', ".-"},
                                        {'b', "-..."},
                                        {'c', "-.-."},
                                        {'d', "-.."},
                                        {'e', "."},
                                        {'f', "..-."},
                                        {'g', "--."},
                                        {'h', "...."},
                                        {'i', ".."},
                                        {'j', ".---"},
                                        {'k', "-.-"},
                                        {'l', ".-.."},
                                        {'m', "--"},
                                        {'n', "-."},
                                        {'o', "---"},
                                        {'p', ".--."},
                                        {'q', "--.-"},
                                        {'r', ".-."},
                                        {'s', "..."},
                                        {'t', "-"},
                                        {'u', "..-"},
                                        {'v', "...-"},
                                        {'w', ".--"},
                                        {'x', "-..-"},
                                        {'y', "-.--"},
                                        {'z', "--.."},
                                        {'.', ".-.-.-"},
                                        {',', "--..--"},
                                        {'!', "-.-.--"}
                                    };
            
            this.encMapRusLet = new Dictionary<char, string>()
                                    {
                                        {'а', ".-"},
                                        {'б', "-..."},
                                        {'в', ".--"},
                                        {'г', "--."},
                                        {'д', "-.."},
                                        {'е', "."},
                                        {'ё', "."},
                                        {'ж', "...-"},
                                        {'з', "--.."},
                                        {'и', ".."},
                                        {'й', ".---"},
                                        {'к', "-.-"},
                                        {'л', ".-.."},
                                        {'м', "--"},
                                        {'н', "-."},
                                        {'о', "---"},
                                        {'п', ".--."},
                                        {'р', ".-."},
                                        {'с', "..."},
                                        {'т', "-"},
                                        {'у', "..-"},
                                        {'ф', "..-."},
                                        {'х', "...."},
                                        {'ц', "-.-."},
                                        {'ч', "---."},
                                        {'ш', "----"},
                                        {'щ', "--.-"},
                                        {'ъ', "--.--"},
                                        {'ы', "-.--"},
                                        {'ь', "-..-"},
                                        {'э', "..-.."},
                                        {'ю', "..--"},
                                        {'я', ".-.-"},
                                        {'.', "......"},
                                        {',', ".-.-.-"},
                                        {'!', "--..--"}
                                    };
            this.encMapSymbols  = new Dictionary<char, string>()
                                    {
                                        {'1', ".----"},
                                        {'2', "..---"},
                                        {'3', "...--"},
                                        {'4', "....-"},
                                        {'5', "....."},
                                        {'6', "-...."},
                                        {'7', "--..."},
                                        {'8', "---.."},
                                        {'9', "----."},
                                        {'0', "-----"},
                                        {':', "---..."},
                                        {';', "-.-.-."},
                                        {'(', "-.--."},
                                        {')', "-.--.-"},
                                        {'\'', ".----."},
                                        {'"', ".-..-."},
                                        {'-', "-....-"},
                                        {'/', "-..-."},
                                        {'?', "..--.."},
                                        {'\n', "-...-"},
                                        {'^', "........"},
                                        {'@', ".--.-."},
                                        {'~', "..-.-"},
                                        {' ', TouchPad.LONG_SPACE.ToString()}
                                    };
            this.decMapIntLet = encMapIntLet
                .ToLookup(kp => kp.Value)
                .ToDictionary(g => g.Key, g => g.First().Key.ToString());

            this.decMapRusLet = encMapRusLet
                .ToLookup(kp => kp.Value)
                .ToDictionary(g => g.Key, g => g.First().Key.ToString());
            this.decMapSymbols = encMapSymbols
                .ToLookup(kp => kp.Value)
                .ToDictionary(g => g.Key, g => g.First().Key.ToString());
        }
        
        public void SetText(string text)
        {
            this.decodedText = text;
            this.Encode();
        }

        public void SetMorse(string text)
        {
            StringBuilder sb = new StringBuilder(text);
            int pos = 0;
            while(pos < sb.Length)
            {
                if (sb[pos] != '-' && sb[pos] != '.' && sb[pos] != ' ')
                {
                    sb.Remove(pos, 1);
                }
                else
                    pos++;
            }
            encodedText = sb.ToString();
            this.Decode();
        }
        private void Encode()
        {
            StringBuilder strBuilder = new StringBuilder();
            int pos = 0;
            while (pos < decodedText.Length)
            {
                char currChar = Char.ToLower(decodedText[pos]);
                if (this.Locale == LOCALE.INT)
                {
                    if (this.encMapIntLet.ContainsKey(currChar))
                    {
                        strBuilder.Append(this.encMapIntLet[currChar]);
                    }
                    else
                    {
                        if (this.encMapSymbols.ContainsKey(currChar))
                        {
                            strBuilder.Append(this.encMapSymbols[currChar]);
                        }
                    }
                }
                if (this.Locale == LOCALE.RUS)
                {
                    if (this.encMapRusLet.ContainsKey(currChar))
                    {
                        strBuilder.Append(this.encMapRusLet[currChar]);
                    }
                    else
                    {
                        if (this.encMapSymbols.ContainsKey(currChar))
                        {
                            strBuilder.Append(this.encMapSymbols[currChar]);
                        }
                    }
                }
                if (this.Locale == LOCALE.ALL)
                {
                    //cause period and comma has different Morse code for each locale, it need extra check
                    if(currChar == '.' || currChar == ',')
                    {
                        if(this.localeByDefault == LOCALE.RUS)
                        {
                            strBuilder.Append(encMapRusLet[currChar]);
                        }
                        else
                        {
                            strBuilder.Append(encMapIntLet[currChar]);
                        }
                    }
                    else
                    {
                        if (this.encMapIntLet.ContainsKey(currChar))
                        {
                            strBuilder.Append(this.encMapIntLet[currChar]);
                        }
                        else
                        {
                            if (this.encMapRusLet.ContainsKey(currChar))
                            {
                                strBuilder.Append(this.encMapRusLet[currChar]);
                            }
                            else
                            {
                                if (this.encMapSymbols.ContainsKey(currChar))
                                {
                                    strBuilder.Append(this.encMapSymbols[currChar]);
                                }
                            }
                        }
                    }
                }
                pos++;
                strBuilder.Append(' ');
            }
            this.encodedText = strBuilder.ToString().Trim();
        }
        private void Decode()
        {
            StringBuilder strBuilder = new StringBuilder();
            string[] morseCodes = this.encodedText.Split(new[] {' '});
            if (this.Locale == LOCALE.INT)
            {
                foreach (string morseCode in morseCodes)
                {
                    if (this.decMapIntLet.ContainsKey(morseCode))
                    {
                        strBuilder.Append(this.decMapIntLet[morseCode]);
                    }
                    else if (this.decMapSymbols.ContainsKey(morseCode))
                    {
                        strBuilder.Append(this.decMapSymbols[morseCode]);
                    }
                }
            }
            if(this.Locale == LOCALE.RUS)
            {
                foreach (string morseCode in morseCodes)
                {
                    if (this.decMapRusLet.ContainsKey(morseCode))
                    {
                        strBuilder.Append(this.decMapRusLet[morseCode]);
                    }
                    else if (this.decMapSymbols.ContainsKey(morseCode))
                    {
                        strBuilder.Append(this.decMapSymbols[morseCode]);
                    }
                }
            }
            this.decodedText = strBuilder.ToString().Trim();
        }
        public string GetEncodedText()
        {
            return this.encodedText;
        }
        public string GetDecodedText()
        {
            return this.decodedText;
        }
    }
}
