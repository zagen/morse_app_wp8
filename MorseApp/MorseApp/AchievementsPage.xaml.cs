﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MorseApp.General;
using Newtonsoft.Json;
using System.Diagnostics;
using MorseApp.Achievements;
using MorseApp.Resources;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;

namespace MorseApp
{
    public partial class AchievementsPage : PhoneApplicationPage
    {
        private List<Achievement> achievements;
        public AchievementsPage()
        {
            InitializeComponent();
            this.AddAds(LayoutRoot, 2);
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);


            string data = "";


            if (NavigationContext.QueryString.TryGetValue("NewAchievements", out data))
            {
                this.achievements = JsonConvert.DeserializeObject<List<Achievement>>(data);
                this.AchievementPageTitleText.Text = AppResources.NewAchievement;
            }
            else
            {
                this.achievements = new AchievementsManager().Achieved();
            }

            if (achievements.Count > 0)
            {
                PositionView.Total = achievements.Count;
                PositionView.CurrentPosition = PositionView.Total - 1;
                Next();
            }
        }
        private void Next()
        {
            AchievementSlideIn.Begin();
            if (this.achievements.Count <= 0)
                return;

            PositionView.CurrentPosition++;
            
            Achievement achievement = this.achievements[PositionView.CurrentPosition];
            AchievementTitleText.Text = achievement.Name;
            AchievementDescription.Text = achievement.Description;
            BitmapImage trophyImage = new BitmapImage();
            trophyImage.SetSource(Application.GetResourceStream(new Uri(achievement.ImagePath, UriKind.Relative)).Stream);
            TrophyImage.Source = trophyImage;

        }
        private void Previous()
        {
            AchievementSlideOut.Begin();
            if (this.achievements.Count <= 0)
                return;

            PositionView.CurrentPosition--;
                        
            Achievement achievement = this.achievements[PositionView.CurrentPosition];
            AchievementTitleText.Text = achievement.Name;
            AchievementDescription.Text = achievement.Description;
            BitmapImage trophyImage = new BitmapImage();
            trophyImage.SetSource(Application.GetResourceStream(new Uri(achievement.ImagePath, UriKind.Relative)).Stream);
            TrophyImage.Source = trophyImage;

        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            Next();
        }

        private void PreviousButton_Click(object sender, RoutedEventArgs e)
        {
            Previous();
        }

        private void OnFlick(object sender, FlickGestureEventArgs e)
        {
           if (e.HorizontalVelocity < 0)
           {
               Next();
           }

           // User flicked towards right
           if (e.HorizontalVelocity > 0)
           {
               // Load the previous image
               Previous();
           }
        }


        private string AppUrl = "http://zagen.zz.mu/Morse-app/";

        private void RangeUpHold(object sender, GestureEventArgs e)
        {
            ShareStatusTask shareStatusTask = new ShareStatusTask();

            shareStatusTask.Status = string.Format(AppResources.ShareAchievementText, achievements[PositionView.CurrentPosition].Name, AppUrl);

            shareStatusTask.Show();
        }

    }
}