﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MorseApp.General;

namespace MorseApp
{
    public partial class TransmitExersPage : PhoneApplicationPage
    {
        public TransmitExersPage()
        {
            InitializeComponent();
            this.AddAds(LayoutRoot, 2);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }
    }
}