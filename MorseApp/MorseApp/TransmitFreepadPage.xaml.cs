﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MorseApp.General;
using System.Text;
using MorseApp.Resources;
using Microsoft.Phone.Tasks;

namespace MorseApp
{
    public partial class TransmitFreepadPage : PhoneApplicationPage
    {
        private TextCodec Codec = new TextCodec();
        private StringBuilder morseCodeStringBuilder = new StringBuilder();

        public TransmitFreepadPage()
        {
            InitializeComponent();
            this.AddAds(LayoutRoot, 2);
            this.touchPad.MorseCodeInputed = InputedCode;

        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }

        private void InputedCode(char code)
        {
            if (code == TouchPad.LONG_SPACE)
            {
                MessageText.Text += " ";
            }
            else if (code == ' ')
            {
                if (morseCodeStringBuilder.Length > 0)
                {
                    this.Codec.SetMorse(morseCodeStringBuilder.ToString());
                    MessageText.Text += this.Codec.GetDecodedText();
                    this.morseCodeStringBuilder.Clear();
                }
            }
            else
            {
                morseCodeStringBuilder.Append(code);
            }

            MessageScrollViewer.ScrollToHorizontalOffset(MessageText.ActualWidth + 10);
        }
        private void UpdateUi()
        {
            if (Codec.Locale == TextCodec.LOCALE.RUS)
            {
                LocaleText.Text = AppResources.Cyr;
            }
            else
            {
                LocaleText.Text = AppResources.Inter;
            }
        }

        private void ChangeLocaleButton_Click(object sender, RoutedEventArgs e)
        {
            if (Codec.Locale == TextCodec.LOCALE.RUS)
            {
                Codec.Locale = TextCodec.LOCALE.INT;
            }
            else
            {
                Codec.Locale = TextCodec.LOCALE.RUS;
            }
            UpdateUi();
        }

        private void BackspaceButton_Click(object sender, RoutedEventArgs e)
        {
            if(this.MessageText.Text.Length > 0)
                this.MessageText.Text = this.MessageText.Text.Substring(0, this.MessageText.Text.Length - 1);
        }

        private void ClearMessageButton_Click(object sender, RoutedEventArgs e)
        {
            if(MessageBox.Show("", AppResources.CleanMessageSure, MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                this.MessageText.Text = "";
            }
        }

        private void ShareButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.MessageText.Text.Length > 0)
            {
                ShareStatusTask shareStatusTask = new ShareStatusTask();

                shareStatusTask.Status = this.MessageText.Text;

                shareStatusTask.Show();
            }
        }
    }
}