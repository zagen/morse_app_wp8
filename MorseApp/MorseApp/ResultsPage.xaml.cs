﻿using System;
using System.Windows;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using MorseApp.Resources;
using MorseApp.General;
namespace MorseApp
{
    public partial class ResultsPage : PhoneApplicationPage
    {
        private ObservableCollection<ResultRecord> resultsVM ;
        private bool IsGoToMainPage = true;
        public ResultsPage()
        {
            InitializeComponent();
            this.AddAds(LayoutRoot, 2);
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            this.Back();
        }
        private void Back()
        {
            if (IsGoToMainPage)
            {
                this.NavigationService.RemoveBackEntry();
                this.NavigationService.RemoveBackEntry();
            }            
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            
            string data = "";
            string GoToMainPage = "";

            if (NavigationContext.QueryString.TryGetValue("data", out data))
            {
                this.resultsVM = JsonConvert.DeserializeObject<ObservableCollection<ResultRecord>>(data);
                this.list.ItemsSource = this.resultsVM;
                Statistics stat = new Statistics(this.resultsVM);
                this.TotalScoreText.Text = String.Format("{0}:\n{1}-{2}\n{3}-100%", AppResources.TotalScore, stat.CorrectCount, stat.TotalCount, stat.CorrectPercent);
            }
            if(NavigationContext.QueryString.TryGetValue("ReturnToMainMenu", out GoToMainPage))
            {
                bool.TryParse(GoToMainPage, out this.IsGoToMainPage);
            }

        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            base.OnBackKeyPress(e);
            this.Back();
        }
    }

    public class ResultRecord
    {
        public string Correct { get; set; }
        public string Inputed { get; set; }
    }
    public class Statistics
    {
        public int TotalCount { get; set; }
        public int CorrectCount { get; set; }
        public double CorrectPercent
        {
            get
            {
                return Math.Round((double)CorrectCount / (double)TotalCount * 100, 2);
            }
        }
        public Statistics(ObservableCollection<ResultRecord> data)
        {
            foreach(ResultRecord record in data)
            {
                this.TotalCount += record.Correct.Length;
                this.CorrectCount += CountOfMatchingSymbols(record.Correct, record.Inputed);
            }
        }
        private int CountOfMatchingSymbols(string correct, string inputed)
        {
            int count = 0;
            int minLength = (correct.Length < inputed.Length) ? correct.Length : inputed.Length;
            for (int i = 0; i < minLength; i++)
            {
                if (correct[i] == inputed[i])
                    count++;
            }
            return count;
        }
    }
}
