﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MorseApp.Resources;
using MorseApp.General;
using System.Diagnostics;

namespace MorseApp.Achievements
{
    public class AchievementsManager
    {
        private  List<Achievement> AllAchievements = new List<Achievement>()
        {
            new Achievement
            {
                CodeName = "PersonalRecord",
                Name = AppResources.TrophyPersonalRecordName,
                Description = AppResources.TrophyPersonalRecordDesc,
                ImagePath = @"Assets/Achievements/NewRecordTrophy.png",
                Value = 0
            },
            new Achievement
            {
                CodeName = "NoMistake",
                Name = AppResources.TrophyNomistakeName,
                Description = AppResources.TrophyNomistakeDesc,
                ImagePath = @"Assets/Achievements/NoMistakeTrophy.png",
                Value = 10
            },
            new Achievement
            {
                CodeName = "Newbie",
                Name = AppResources.TrophyNewbieName,
                Description = AppResources.TrophyNewbieDesc,
                ImagePath = @"Assets/Achievements/FirstTimeTrophy.png",
                Value = 5
            },
            new Achievement
            {
                CodeName = "Apprentice",
                Name = AppResources.TrophyApprenticeName,
                Description = AppResources.TrophyApprenticeDesc,
                ImagePath = @"Assets/Achievements/SubmasterTrophy.png",
                Value = 15
            },
            new Achievement
            {
                CodeName = "Master",
                Name = AppResources.TrophyMasterName,
                Description = AppResources.TrophyMasterDesc,
                ImagePath = @"Assets/Achievements/MasterTrophy.png",
                Value = 30
            },
            new Achievement
            {
                CodeName = "GrandMaster",
                Name = AppResources.TrophyGrandMasterName,
                Description = AppResources.TrophyGrandMasterDesc,
                ImagePath = @"Assets/Achievements/GrandmasterTrophy.png",
                Value = 50
            }
        };

        public List<Achievement> New(int correct, int inputed)
        {
            List<Achievement> newAchivements = new List<Achievement>();
            List<string> codeNamesAchieved = Settings.Instance.Achievements;

            if(correct > Settings.Instance.PersonalRecord)
            {
                Achievement personalRecordAchievement = AllAchievements.Find(i => i.CodeName.Equals("PersonalRecord"));
                personalRecordAchievement.Value = correct;
                Settings.Instance.PersonalRecord = correct;
                Settings.Instance.Sync();
                if(!codeNamesAchieved.Contains("PersonalRecord"))
                {
                    codeNamesAchieved.Add("PersonalRecord");
                }
                personalRecordAchievement.Description = string.Format(AppResources.TrophyPersonalRecordDesc, correct);
                newAchivements.Add(personalRecordAchievement);
            }
            foreach (Achievement achievement in AllAchievements)
            {
                if (!achievement.CodeName.Equals("PersonalRecord") && !achievement.CodeName.Equals("NoMistake"))
                {
                    if (correct > achievement.Value && !codeNamesAchieved.Contains(achievement.CodeName))
                    {
                        codeNamesAchieved.Add(achievement.CodeName);
                        newAchivements.Add(achievement);
                    }
                }
                else if (achievement.CodeName.Equals("NoMistake"))
                {
                    if (!codeNamesAchieved.Contains(achievement.CodeName) && correct == inputed)
                    {
                        codeNamesAchieved.Add(achievement.CodeName);
                        newAchivements.Add(achievement);
                    }
                }
            }
            Settings.Instance.Achievements = codeNamesAchieved;
            Settings.Instance.Sync();
            return newAchivements;
        }

        public List<Achievement> Achieved()
        {
            List<Achievement> achieved = new List<Achievement>();

            foreach (string CodeName in Settings.Instance.Achievements)
            {
                Achievement achievement = AllAchievements.Find(i => i.CodeName.Equals(CodeName));
                if (CodeName.Equals("PersonalRecord"))
                {
                    achievement.Value = Settings.Instance.PersonalRecord;
                    achievement.Description = string.Format(AppResources.TrophyPersonalRecordDesc, achievement.Value);
                }
                achieved.Add(achievement);
            }
            return achieved;
        }
    }
}
