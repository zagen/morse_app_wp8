﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorseApp.Achievements
{
    public class Achievement
    {
        public string CodeName { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
    }
}
