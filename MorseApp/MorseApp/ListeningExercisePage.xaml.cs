﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MorseApp.General;
using MorseApp.Transmitters;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using MorseApp.Resources;
using System.Windows.Input;
using System.Text;
using System.Windows.Documents;

namespace MorseApp
{
    public partial class ListeningExercisePage : PhoneApplicationPage
    {
        private Transmitter _Transmitter;
        private ObservableCollection<ResultRecord> _ResultsData = new ObservableCollection<ResultRecord>();
        private Generator _Generator;
        private string _CurrentText;
        private Settings _Settings;

        public ListeningExercisePage()
        {
            InitializeComponent();
            this.AddAds(LayoutRoot, 2);
            _Transmitter = Transmitter.CreateCurrentTransmitter();
            _Transmitter.ParentPage = this;
            _Transmitter.TransmissionFinished = delegate
            {
                RadioAnimationStoryBoard.Stop();
            };
            _Transmitter.ParentPage = this;
            _Generator = new Generator();
            _Settings = Settings.Instance;
            _Settings.PropertyChanged += _Settings_PropertyChanged;
            Reload();

        }

        void _Settings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("SequenceType"))
            {
                _Generator.Type = _Settings.SequenceType;
                Reload();
            }
            else if (e.PropertyName.Equals("IntAbc"))
            {
                _Generator.SetAbc(_Settings.IntAbc);
                Reload();
            }
            else if (e.PropertyName.Equals("RusAbc"))
            {
                _Generator.SetAbc(_Settings.RusAbc);
                Reload();
            }
        }

        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            if (_Transmitter.IsPlaying)
            {
                _Transmitter.Stop();
            }
            else
            {
                RadioAnimationStoryBoard.Begin();
                _Transmitter.Transmit(_CurrentText);
                InputBox.Focus();
            }
            

        }
        private void Reload(string Message = "")
        {
            _Transmitter.Stop();
            CorrectAnswerValuesText.Opacity = 0;
            CorrectAnswerValuesText.Text = "";

            InputBox.Text = "";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Settings.Instance.GroupOnReceive; i++)
            {
                sb.Append(_Generator.ToString());
                sb.Append(" ");
            }
            _CurrentText = sb.ToString().Trim().ToUpper();
            StatusText.ShowPopup(Message + AppResources.NewSequenceGenerated);
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            _Transmitter.Stop();
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            _Transmitter.Stop();
            if (_ResultsData.Count <= 0)
            {
                if (InputBox.Text.Trim().Length > 0)
                {
                    CheckInput();
                    this.NavigationService.Navigate(new Uri("/ResultsPage.xaml?data=" + JsonConvert.SerializeObject(this._ResultsData), UriKind.Relative));
                }
                else if(this.NavigationService.CanGoBack)
                {
                    this.NavigationService.GoBack();
                }
                
            }
            else
            {
                this.NavigationService.Navigate(new Uri("/ResultsPage.xaml?data=" + JsonConvert.SerializeObject(this._ResultsData), UriKind.Relative));
            }
        }

        private void ReloadButton_Click(object sender, RoutedEventArgs e)
        {
            Reload();
        }

        private void InputBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key.Equals(Key.Enter) && InputBox.Text.Trim().Length > 0)
            {
                _Transmitter.Stop();
                CheckInput();
                this.Focus();
            }
        }

        private void CheckInput()
        {
            string[] inputedGroups = InputBox.Text.Trim().ToUpper().Split(' ');
            string[] correctGroups = _CurrentText.Split(' ');
            bool IsAnswerCorrect = true;
            CorrectAnswerValuesText.Text = "";

            for (int i = 0; i < correctGroups.Length; i++)
            {
                string inputed = "";
                string correct = correctGroups[i];

                if(i < inputedGroups.Length)
                {
                    inputed = inputedGroups[i];
                }
                
                if (CorrectAnswerValuesText.CompareTextBlockWithStringWithoutClearInlines(correct, inputed) != 0)
                {
                    IsAnswerCorrect = false;
                }
                CorrectAnswerValuesText.Inlines.Add(Environment.NewLine);
                _ResultsData.Add(new ResultRecord
                {
                    Correct = correct,
                    Inputed = inputed
                });

            }
            if (IsAnswerCorrect)
            {
                CorrectAnswerValuesText.Text = "";
                CorrectAnswerTitleText.Opacity = 0;
                CorrectAnswerValuesText.Opacity = 0;
                Reload(AppResources.AnswerIsCorrect + " ");
            }
            else
            {
                StatusText.ShowPopup(AppResources.WrongAnswer);
                CorrectAnswerTitleText.Opacity = 1;
                CorrectAnswerValuesText.Opacity = 1;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (_Transmitter.IsPlaying)
            {
                _Transmitter.Stop();
            }
            this.NavigationService.Navigate(new Uri("/HandbookListPage.xaml", UriKind.Relative));
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {

            if (_Transmitter.IsPlaying)
            {
                _Transmitter.Stop();
            }
            this.NavigationService.Navigate(new Uri("/SelectSymbolsPage.xaml", UriKind.Relative));
        }
    }
}