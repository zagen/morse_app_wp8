﻿using System;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using GoogleAds;
using MorseApp.General;
using MorseApp.Resources;
using Windows.ApplicationModel.Store;
using Store = Windows.ApplicationModel.Store;


namespace MorseApp
{
    public partial class SettingsPage : PhoneApplicationPage
    {

        private Settings settings;
        public SettingsPage()
        {
            InitializeComponent();
            this.AddAds(LayoutRoot, 2);
            this.settings = Settings.Instance;
            if (this.settings.AdsRemoved)
            {
                RemoveAdsBtn.IsEnabled = false;
            }
            else
            {
                RemoveAdsBtn.IsEnabled = true;
            }
            UpdateUI();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.NavigationService.CanGoBack)
            {
                this.NavigationService.GoBack();
            }
        }


        private void UpdateUI()
        {
            //set Locale
            if (this.settings.Locale == TextCodec.LOCALE.INT)
            {
                this.LocaleIntRadio.IsChecked = true;
            }
            else
            {
                this.LocaleRusRadio.IsChecked = true;
            }

            switch (this.settings.Transmitter)
            {
                case 1:
                    this.TransmitterScreenRadio.IsChecked = true;
                    break;
                case 2:
                    this.TransmitterVibroRadio.IsChecked = true;
                    break;
                case 0:
                default:
                    this.TransmitterToneRadio.IsChecked = true;
                    break;
            }
            this.ShowMnemonicsChbox.IsChecked = settings.IsMnemonicsShowed;
            this.ShowInformationChbox.IsChecked = settings.IsInformationMessagesShowing;
            this.BrightnessSlider.Value = settings.Brightness;
            this.BrightnessText.Text = String.Format("{0}: {1}", AppResources.Brightness, settings.Brightness);

            this.GeneratorLengthSlider.Value = settings.GeneratorLength;
            this.GeneratorLengthText.Text = String.Format("{0}: {1}", AppResources.GeneratorLength, settings.GeneratorLength);
            
            this.SignalDurationSlider.Value = settings.SignalDuration;
            this.SignalDurationText.Text = String.Format("{0}: {1}", AppResources.SignalDuration, settings.SignalDuration);

            this.GroupsNumberSlider.Value = settings.GroupOnReceive;
            this.GroupsNumberText.Text = String.Format("{0}: {1}", AppResources.GroupNumberOnReceiving, settings.GroupOnReceive);

            this.FrequenceSlider.Value = settings.Frequence;
            this.FrequenceText.Text = String.Format("{0}: {1}", AppResources.FrequenceSetting, settings.Frequence);
        }

        private void SettingsSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sender.GetType().Name == "Slider" && settings != null)
            {
                Slider slider = (Slider)sender;
                switch (slider.Name)
                {
                    case "SignalDurationSlider":
                        this.settings.SignalDuration = Convert.ToInt32(slider.Value);
                        break;
                    case "BrightnessSlider":
                        this.settings.Brightness = Convert.ToInt32(slider.Value);
                        break;
                    case "GeneratorLengthSlider":
                        this.settings.GeneratorLength = Convert.ToInt32(slider.Value);
                        break;
                    case "GroupsNumberSlider":
                        this.settings.GroupOnReceive = Convert.ToInt32(slider.Value);
                        break;
                    case "FrequenceSlider":
                        int step = 40;
                        slider.Value = (e.NewValue % step != 0) ? (step - e.NewValue % step) + e.NewValue : e.NewValue;
                        this.settings.Frequence = Convert.ToInt32(slider.Value);
                        break;
                }
                settings.Sync();
                UpdateUI();
            }

        }

        private void SettingsCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            if (sender.GetType().Name == "CheckBox" && settings != null)
            {
                CheckBox checkbox = (CheckBox)sender;
                switch (checkbox.Name)
                {
                    case "ShowMnemonicsChbox":
                        this.settings.IsMnemonicsShowed = checkbox.IsChecked ?? true;
                        break;
                    case "ShowInformationChbox":
                        this.settings.IsInformationMessagesShowing = checkbox.IsChecked ?? true;
                        break;
                }
                settings.Sync();
                UpdateUI();
                
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            switch ((sender as RadioButton).Name)
            {
                case "LocaleIntRadio":
                    this.settings.Locale = TextCodec.LOCALE.INT;
                    break;
                case "LocaleRusRadio":
                    this.settings.Locale = TextCodec.LOCALE.RUS;
                    break;
                case "TransmitterToneRadio":
                    this.settings.Transmitter = 0;
                    break;
                case "TransmitterScreenRadio":
                    this.settings.Transmitter = 1;
                    break;
                case "TransmitterVibroRadio":
                    this.settings.Transmitter = 2;
                    break;
            }
            settings.Sync();
            UpdateUI();
        }

        async private void  RemoveAdsBtn_Click(object sender, RoutedEventArgs e)
        {
            string productID = "remove_ads";
            if (!Store.CurrentApp.LicenseInformation.ProductLicenses[productID].IsActive)
            {
                try
                {
                    await CurrentApp.RequestProductPurchaseAsync(productID, false);
                    ProductLicense productLicense = null;
                    if (CurrentApp.LicenseInformation.ProductLicenses.TryGetValue(productID, out productLicense))
                    {
                        if (productLicense.IsActive)
                        {
                            MessageBox.Show(AppResources.PurchaseThanks);
                            CurrentApp.ReportProductFulfillment(productID);
                            Settings.Instance.AdsRemoved = true;
                            RemoveAdsBtn.IsEnabled = false;
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(AppResources.PurchaseError);
                    Settings.Instance.AdsRemoved = false;
                }
            }
            else
            {
                MessageBox.Show(AppResources.PurchaseRestored);
                RemoveAdsBtn.IsEnabled = false;
                Settings.Instance.AdsRemoved = true;
                Settings.Instance.Sync();
            }
     

        }
        
    }

}