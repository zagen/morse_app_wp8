﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using MorseApp.General;
using System.Diagnostics;
using System.Threading;

namespace MorseAppTest
{
    [TestClass]
    public class WordAccessorTest
    {
        [TestMethod]
        public void TestWordAccessorClass_generatingRandomWordPositive()
        {
            WordAccessor wa = WordAccessor.Instance;
            Thread.Sleep(200);
            string word = wa.GetRandomWord(TextCodec.LOCALE.INT);
            Debug.WriteLine("{0} - random word", word);
            Assert.AreNotEqual("", word);
        }
        [TestMethod]
        public void TestWordAccessorClass_generatingRandomWordRusPositive()
        {
            WordAccessor wa = WordAccessor.Instance;
            Thread.Sleep(200);
            string word = wa.GetRandomWord(TextCodec.LOCALE.RUS);
            Debug.WriteLine("{0} - random word", word);
            Assert.AreNotEqual("", word);
        }
    }

      
}
