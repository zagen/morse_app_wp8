﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using MorseApp.General;

namespace MorseAppTest
{
    [TestClass]
    public class TextCodecTest
    {
        [TestMethod]
        public void TestTextCodecClass_encodingMorseCode_positive()
        {
            TextCodec codec = new TextCodec();
            codec.Locale = TextCodec.LOCALE.INT;
            codec.SetText("sos");
            Assert.AreEqual("... --- ...", codec.GetEncodedText());
            
        }

        [TestMethod]
        public void TestTextCodecClass_encodingMorseCodeRus_positive()
        {
            TextCodec codec = new TextCodec();
            codec.Locale = TextCodec.LOCALE.RUS;
            codec.SetText("пес");
            Assert.AreEqual(".--. . ...", codec.GetEncodedText());
        }

        [TestMethod]
        public void TestTextCodecClass_decodingMorseCodeRus_positive()
        {
            TextCodec codec = new TextCodec();
            codec.Locale = TextCodec.LOCALE.RUS;
            codec.SetMorse(".--. . ...");
            Assert.AreEqual("пес", codec.GetDecodedText());
        }

        [TestMethod]
        public void TestTextCodecClass_decodingMorseCode_positive()
        {
            TextCodec codec = new TextCodec();
            codec.Locale = TextCodec.LOCALE.INT;
            codec.SetMorse("... --- ...");
            Assert.AreEqual("sos", codec.GetDecodedText());
        }
    }
}
