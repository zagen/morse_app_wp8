﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using MorseApp.General;
using System.Diagnostics;

namespace MorseAppTest
{
    [TestClass]
    public class GeneratorTests
    {
        [TestMethod]
        public void TestGeneratorClass_generatedSizePositive()
        {
            Generator generator = new Generator(5, TextCodec.LOCALE.INT);
            Debug.WriteLine("{0} - generated ", generator.ToString());
            Assert.AreEqual(generator.ToString().Length, 5);
        }

        [TestMethod]
        public void TestGeneratorClass_generatedAbcPositive()
        {
            Generator generator = new Generator(5, TextCodec.LOCALE.INT);
            generator.SetAbc("ant");
            string seq = generator.ToString();
            foreach (char currChar  in seq)
            {
                if (currChar != 'a' && currChar != 'n' && currChar != 't')
                {
                    Assert.Fail();
                }
            }
        }
    }

}
